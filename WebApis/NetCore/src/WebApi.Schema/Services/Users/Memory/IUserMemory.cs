﻿using WebApi.DTO.Mapping;

namespace WebApi.Schema.Services.Users.Memory
{
    public interface IUserMemory
    {
        string AddUserInMemory(UserDto user);
        UserDto GetUserByToken(string token);
        void RemoveUserByToken(string token);
    }
}
