﻿using Microsoft.Extensions.Caching.Memory;
using System;
using WebApi.DTO.Mapping;
using WebApi.Schema.Crypto;

namespace WebApi.Schema.Services.Users.Memory
{
    public class UserMemory : IUserMemory
    {
        private readonly IMemoryCache _cache;
        private readonly ICoder _coder;

        public UserMemory(IMemoryCache cache, ICoder coder)
        {
            _cache = cache;
            _coder = coder;
        }
        public string AddUserInMemory(UserDto user)
        {
            var token = _coder.GetCodeByValSalt(user.Login);

            _cache.Set(token, user);

            return token;
        }

        public UserDto GetUserByToken(string token)
        {
            if (_cache.TryGetValue(token, out UserDto user))
            {
                return user;
            }
            throw new Exception("User is not found");
        }

        public void RemoveUserByToken(string token)
        {
            if (_cache.TryGetValue(token, out UserDto _))
            {
                _cache.Remove(token);
            }
            else
            {
                throw new Exception("User is not found");
            }
        }
    }
}
