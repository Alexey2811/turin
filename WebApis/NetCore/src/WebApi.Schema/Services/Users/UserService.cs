﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using WebApi.DAL.Entities;
using WebApi.DAL.Repository;
using WebApi.DTO.Mapping;
using WebApi.DTO.Requests;
using WebApi.Schema.Crypto;

namespace WebApi.Schema.Services.Users
{
    public class UserService : IUserService
    {
        private readonly IWebApiRepository<DAL.Entities.Users> _userRepository;
        private readonly IQueryable<DAL.Entities.Users> _userQuery;
        private readonly IMapper _mapper;
        private readonly ICoder _coder;

        public UserService(IWebApiRepository<DAL.Entities.Users> userRepository, 
            IMapper mapper, 
            ICoder coder)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _coder = coder;
            _userQuery = userRepository.GetQuery();
        }

        public async Task<UserDto> AddUserAsync(AddUserModel model)
        {
            var newUser = _mapper.Map<DAL.Entities.Users>(model);
            newUser.PasswordHash = _coder.GetCodeByValSalt(model.Password);

            newUser.LinkUsersRoles.Add(new LinkUsersRoles { 
                RoleId = 1
            });

            _userRepository.Add(newUser);
            await _userRepository.SaveAsync();

            return _mapper.Map<UserDto>(newUser);
        }

        public async Task<UserDto> GetUserByLoginPasswordAsync(AuthorizationModel model)
        {
            var query = await _userQuery
                .Include("LinkUsersRoles.Role")
                .FirstOrDefaultAsync(
                    r => r.Login.Equals(model.Login)
                );

            if (query == default)
                throw new Exception("User is not found");

            var verify = _coder.Verify(model.Password, query.PasswordHash);
            
            if (!verify)
                throw new Exception("User is not found");

            return _mapper.Map<UserDto>(query);
        }

        public async Task UpdateUserAsync(UpdateUserModel model)
        {
            var query = await _userQuery
                .Include("LinkUsersRoles.Role")
                .FirstOrDefaultAsync(r => r.UserId==model.UserId);

            if (query == default)
                throw new Exception("User is not found");

            query.Login = model.Login;
            query.PasswordHash = _coder.GetCodeByValSalt(model.Password);
            
            _userRepository.Update(query);

            await _userRepository.SaveAsync();
        }

        public async Task<UserDto> GetUserByIdAsync(int userId)
        {
            var user = await _userQuery
                .Include("LinkUsersRoles.Role")
                .FirstOrDefaultAsync(r=>r.UserId==userId);
            
            if (user==null)
                throw new Exception("user is not found");

            var userDto = _mapper.Map<UserDto>(user);

            return userDto;
        }

        public async Task<bool> IsExistLogin(string login)
        {
            return await _userQuery.AnyAsync(r => r.Login.Equals(login));
        }
    }
}
