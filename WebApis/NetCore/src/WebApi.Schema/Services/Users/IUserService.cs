﻿using System.Threading.Tasks;
using WebApi.DTO.Mapping;
using WebApi.DTO.Requests;

namespace WebApi.Schema.Services.Users
{
    public interface IUserService
    {
        Task<UserDto> GetUserByLoginPasswordAsync(AuthorizationModel model);
        Task<UserDto> AddUserAsync(AddUserModel model);
        Task UpdateUserAsync(UpdateUserModel model);
        Task<UserDto> GetUserByIdAsync(int userId);
        Task<bool> IsExistLogin(string login);
    }
}
