﻿using Microsoft.Extensions.DependencyInjection;
using WebApi.Schema.Services.Books;
using WebApi.Schema.Services.Chat;
using WebApi.Schema.Services.Chat.Messages;
using WebApi.Schema.Services.Users;
using WebApi.Schema.Services.Users.Memory;

namespace WebApi.Schema.Services
{
    public static class WebApiServicesRegistry
    {
        public static IServiceCollection AddWebApiServices(this IServiceCollection collection)
        {
            collection.AddTransient<IUserService, UserService>();
            collection.AddTransient<IUserMemory, UserMemory>();

            collection.AddTransient<IBookService, BookService>();

            collection.AddTransient<IChatService, ChatService>();
            collection.AddTransient<IChatMessagesService, ChatMessagesService>();

            collection.AddTransient<MigrationService>();

            return collection;
        }
    }
}
