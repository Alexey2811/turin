using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using WebApi.DAL.FakeData;
using WebApi.Schema.Crypto;

namespace WebApi.Schema.Services
{
    public class MigrationService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ILoggerFactory _loggerFactory;
        private readonly ICoder _coder;

        public MigrationService(IServiceProvider serviceProvider, ILoggerFactory loggerFactory, ICoder  coder)
        {
            _serviceProvider = serviceProvider;
            _loggerFactory = loggerFactory;
            _coder = coder;
        }
        public async Task Migrate()
        {
            await DataWriter.WriteAsync(_serviceProvider, _loggerFactory);
        }
    }
}