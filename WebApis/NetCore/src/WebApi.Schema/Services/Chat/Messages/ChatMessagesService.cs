﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.DAL.Repository;
using WebApi.DTO;
using WebApi.DTO.Mapping;
using WebApi.DTO.Requests;

namespace WebApi.Schema.Services.Chat.Messages
{
    public class ChatMessagesService : IChatMessagesService
    {
        private readonly IWebApiRepository<DAL.Entities.ChatMessages> _chatRepository;
        private readonly IQueryable<DAL.Entities.ChatMessages> _messagesQuery;
        private readonly IMapper _mapper;

        public ChatMessagesService(IWebApiRepository<DAL.Entities.ChatMessages> chatRepository,
            IMapper mapper)
        {
            _chatRepository = chatRepository;
            _mapper = mapper;
            _messagesQuery = chatRepository.GetQuery();
        }
        public async Task<ChatMessagesDto> AddChatMessagesAsync(AddChatMessageModel model)
        {
            var newMessage = _mapper.Map<DAL.Entities.ChatMessages>(model);

            _chatRepository.Add(newMessage);
            await _chatRepository.SaveAsync();

            return _mapper.Map<ChatMessagesDto>(newMessage);
        }

        public async Task DeleteMessageAsync(int messageId)
        {
            var chat = await _messagesQuery.FirstOrDefaultAsync(r => r.ChatMessageId == messageId);

            if (chat == default)
                throw new Exception("chatMessages is not found");

            _chatRepository.Delete(chat);

            await _chatRepository.SaveAsync();
        }

        public async Task<ChatMessagesDto> GetMesssageByIdAsync(int chatMessageId)
        {
            var chat = await _messagesQuery.FirstOrDefaultAsync(r=>r.ChatMessageId==chatMessageId);
            
            if (chat==null)
                throw new Exception("chatMessage is not found");

            var chatMessageDto = _mapper.Map<ChatMessagesDto>(chat);

            return chatMessageDto;
        }

        public async Task<ResponsePaginationModel<ChatMessagesDto>> GetMessagesAsync(RequestPaginationWithChatIdModel request)
        {
            var chatQuery = _messagesQuery.Where(r => r.ChatId == request.ChatId);

            var result = new ResponsePaginationModel<ChatMessagesDto>
            {
                Count = await chatQuery.CountAsync()
            };

            var query = chatQuery
                .Include(r => r.Creator)
                .OrderBy(r => r.ChatMessageId)
                .Skip(request.Page * request.Size)
                .Take(request.Size);

            result.Items = _mapper.Map<IEnumerable<ChatMessagesDto>>(query);

            return result;
        }
    }
}
