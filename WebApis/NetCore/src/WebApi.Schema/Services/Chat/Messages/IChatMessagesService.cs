﻿using System.Threading.Tasks;
using WebApi.DTO;
using WebApi.DTO.Mapping;
using WebApi.DTO.Requests;

namespace WebApi.Schema.Services.Chat.Messages
{
    public interface IChatMessagesService
    {
        Task<ResponsePaginationModel<ChatMessagesDto>> GetMessagesAsync(RequestPaginationWithChatIdModel request);
        Task<ChatMessagesDto> AddChatMessagesAsync(AddChatMessageModel model);
        Task DeleteMessageAsync(int messageId);
        Task<ChatMessagesDto> GetMesssageByIdAsync(int chatMessageId);
    }
}
