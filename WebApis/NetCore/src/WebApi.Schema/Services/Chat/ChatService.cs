﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.DAL.Repository;
using WebApi.DTO;
using WebApi.DTO.Mapping;
using WebApi.DTO.Requests;

namespace WebApi.Schema.Services.Chat
{
    public class ChatService : IChatService
    {
        private readonly IWebApiRepository<DAL.Entities.Chats> _chatRepository;
        private readonly IQueryable<DAL.Entities.Chats> _chatQuery;
        private readonly IMapper _mapper;

        public ChatService(IWebApiRepository<DAL.Entities.Chats> chatRepository,
            IMapper mapper)
        {
            _chatRepository = chatRepository;
            _mapper = mapper;
            _chatQuery = _chatRepository.GetQuery();
        }
        public async Task<ChatDto> AddChatAsync(AddChatModel model)
        {
            var newChat = _mapper.Map<DAL.Entities.Chats>(model);

            _chatRepository.Add(newChat);
            await _chatRepository.SaveAsync();

            return _mapper.Map<ChatDto>(newChat);
        }

        public async Task DeleteChatAsync(int chatId)
        {
            var chat = await _chatQuery.FirstOrDefaultAsync(r => r.ChatId == chatId);

            if (chat == default)
                throw new Exception("chat is not found");

            _chatRepository.Delete(chat);

            await _chatRepository.SaveAsync();
        }

        public async Task<ResponsePaginationModel<ChatDto>> GetAllChatsAsync(RequestPaginationModel request)
        {
            var result = new ResponsePaginationModel<ChatDto>
            {
                Count = await _chatQuery.CountAsync()
            };

            var query = _chatQuery
                .Include(r => r.Creator)
                .OrderByDescending(r=>r.ChatId)
                .Skip(request.Page * request.Size)
                .Take(request.Size);

            result.Items = _mapper.Map<IEnumerable<ChatDto>>(query);

            return result;
        }

        public async Task<ResponsePaginationModel<ChatDto>> GetMyChatsAsync(RequestPaginationWithUserIdModel request)
        {
            var myQuery = _chatQuery.Where(r => r.CreatorId == request.UserId);

            var result = new ResponsePaginationModel<ChatDto>
            {
                Count = await myQuery.CountAsync()
            };

            var query = myQuery
                .Include(r => r.Creator)
                .OrderByDescending(r => r.ChatId)
                .Skip(request.Page * request.Size)
                .Take(request.Size);

            result.Items = _mapper.Map<IEnumerable<ChatDto>>(query);

            return result;
        }

        public async Task UpdateChatAsync(UpdateChatModel model)
        {
            var query = await _chatQuery.FirstOrDefaultAsync(r => r.ChatId == model.ChatId);

            if (query == default)
                throw new Exception("chat is not found");

            query.Name = model.ChatName;

            _chatRepository.Update(query);

            await _chatRepository.SaveAsync();
        }

        public async Task<ChatDto> GetChatByIdAsync(int chatId)
        {
            var chat = await _chatQuery.FirstOrDefaultAsync(r=>r.ChatId==chatId);
            
            if (chat==null)
                throw new Exception("chat is not found");

            var chatDto = _mapper.Map<ChatDto>(chat);

            return chatDto;
        }
    }
}
