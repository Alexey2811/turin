﻿using System.Threading.Tasks;
using WebApi.DTO;
using WebApi.DTO.Mapping;
using WebApi.DTO.Requests;

namespace WebApi.Schema.Services.Chat
{
    public interface IChatService
    {
        Task<ResponsePaginationModel<ChatDto>> GetMyChatsAsync(RequestPaginationWithUserIdModel request);
        Task<ResponsePaginationModel<ChatDto>> GetAllChatsAsync(RequestPaginationModel request);
        Task DeleteChatAsync(int chatId);
        Task<ChatDto> AddChatAsync(AddChatModel model);
        Task UpdateChatAsync(UpdateChatModel model);
        Task<ChatDto> GetChatByIdAsync(int chatId);
    }
}
