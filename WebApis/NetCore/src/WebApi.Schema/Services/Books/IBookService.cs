﻿using System.Threading.Tasks;
using WebApi.DTO;
using WebApi.DTO.Mapping;
using WebApi.DTO.Requests;

namespace WebApi.Schema.Services.Books
{
    public interface IBookService
    {
        Task<ResponsePaginationModel<BookDto>> GetBooksAsync(RequestPaginationModel request);
        Task<BookDto> GetBookByIdAsync(int bookId);
        Task<BookDto> AddBookAsync(AddBookModel model);
        Task UpdateBookAsync(UpdateBookModel model);
        Task DeleteBookAsync(int bookId);
    }
}
