﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.DAL.Repository;
using WebApi.DTO;
using WebApi.DTO.Mapping;
using WebApi.DTO.Requests;

namespace WebApi.Schema.Services.Books
{
    public class BookService : IBookService
    {
        private readonly IWebApiRepository<DAL.Entities.Books> _repository;
        private readonly IQueryable<DAL.Entities.Books> _bookQuery;
        private readonly IMapper _mapper;

        public BookService(IWebApiRepository<DAL.Entities.Books> repository, IMapper mapper)
        {
            _repository = repository;
            _bookQuery = _repository.GetQuery();
            _mapper = mapper;
        }

        public async Task<BookDto> GetBookByIdAsync(int bookId)
        {
            var book = await _bookQuery.FirstOrDefaultAsync(r => r.BookId == bookId);
            if (book == default)
                throw new Exception("book is not found");

            return _mapper.Map<BookDto>(book);
        }

        public async Task<BookDto> AddBookAsync(AddBookModel model)
        {
            var newBook = _mapper.Map<DAL.Entities.Books>(model);

            _repository.Add(newBook);
            await _repository.SaveAsync();

            return _mapper.Map<BookDto>(newBook);
        }

        public async Task DeleteBookAsync(int bookId)
        {
            var item =await _bookQuery.FirstOrDefaultAsync(r => r.BookId == bookId);

            if (item == default) 
                throw new Exception("book is not found");

            _repository.Delete(item);

            await _repository.SaveAsync();
        }

        public async Task<ResponsePaginationModel<BookDto>> GetBooksAsync(RequestPaginationModel request)
        {
            var result = new ResponsePaginationModel<BookDto>
            {
                Count = await _bookQuery.CountAsync()
            };

            var list = await _bookQuery
                .OrderByDescending(r => r.BookId)
                .Skip(request.Page * request.Size)
                .Take(request.Size)
                .ToListAsync();

            result.Items = _mapper.Map<IEnumerable<BookDto>>(list);

            return result;
        }

        public async Task UpdateBookAsync(UpdateBookModel model)
        {
            var query = await _bookQuery.FirstOrDefaultAsync(r=>r.BookId == model.BookId);

            if (query == default)
                throw new Exception("book is not found");

            query.Author = model.Author;
            query.Description = model.Description;
            query.Name = model.Name;
            
            _repository.Update(query);

            await _repository.SaveAsync();
        }
    }
}
