﻿using AutoMapper;
using WebApi.DAL.Entities;
using WebApi.DTO.Mapping;

namespace WebApi.Schema.Mapper.Rules
{
    public class RoleDtoToLinkUsersRoles : Profile
    {
        public RoleDtoToLinkUsersRoles()
        {
            CreateMap<RoleDto, LinkUsersRoles>()
                .ForMember(dest => dest.RoleId, opt => opt.MapFrom(r => r.RoleId));
        }
    }
}
