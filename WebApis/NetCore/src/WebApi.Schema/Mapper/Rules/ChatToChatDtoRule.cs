﻿using AutoMapper;
using WebApi.DAL.Entities;
using WebApi.DTO.Mapping;

namespace WebApi.Schema.Mapper.Rules
{
    public class ChatToChatDtoRule : Profile
    {
        public ChatToChatDtoRule()
        {
            CreateMap<Chats, ChatDto>()
               .ForMember(dest => dest.CreatorId, opt => opt.MapFrom(r => r.CreatorId))
               .ForMember(dest => dest.Name, opt => opt.MapFrom(r => r.Name))
               .ForMember(dest => dest.ChatId, opt => opt.MapFrom(r => r.ChatId))
               .ForMember(dest => dest.CreatorName, opt => opt.MapFrom(r => r.Creator.Login));
        }
    }
}
