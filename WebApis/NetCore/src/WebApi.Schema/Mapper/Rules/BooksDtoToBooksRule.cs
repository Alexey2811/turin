﻿using AutoMapper;
using WebApi.DAL.Entities;
using WebApi.DTO.Mapping;

namespace WebApi.Schema.Mapper.Rules
{
    public class BooksDtoToBooksRule : Profile
    {
        public BooksDtoToBooksRule()
        {
            CreateMap<BookDto, Books>()
                .ForMember(dest => dest.Author, opt => opt.MapFrom(r => r.Author))
                .ForMember(dest => dest.BookId, opt => opt.MapFrom(r => r.BookId))
                .ForMember(dest => dest.CreationDate, opt => opt.MapFrom(r => r.CreationDate))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(r => r.Description))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(r => r.Name));
        }
    }
}
