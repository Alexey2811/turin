﻿using AutoMapper;
using WebApi.DAL.Entities;
using WebApi.DTO.Mapping;

namespace WebApi.Schema.Mapper.Rules
{
    public class ChatMessagesToChatMessagesDtoRule : Profile
    {
        public ChatMessagesToChatMessagesDtoRule()
        {
            CreateMap<ChatMessages, ChatMessagesDto>()
               .ForMember(dest => dest.ChatId, opt => opt.MapFrom(r => r.ChatId))
               .ForMember(dest => dest.ChatMessageId, opt => opt.MapFrom(r => r.ChatMessageId))
               .ForMember(dest => dest.CreatorId, opt => opt.MapFrom(r => r.CreatorId))
               .ForMember(dest => dest.CreatorName, opt => opt.MapFrom(r => r.Creator.Login))
               .ForMember(dest => dest.Message, opt => opt.MapFrom(r => r.Message));
        }
    }
}
