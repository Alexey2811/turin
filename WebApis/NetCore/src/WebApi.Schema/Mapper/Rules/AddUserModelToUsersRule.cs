﻿using AutoMapper;
using WebApi.DAL.Entities;
using WebApi.DTO.Requests;

namespace WebApi.Schema.Mapper.Rules
{
    public class AddUserModelToUsersRule : Profile
    {
        public AddUserModelToUsersRule()
        {
            CreateMap<AddUserModel, Users>()
                .ForMember(dest => dest.Login, opt => opt.MapFrom(r => r.Login));
        }
    }
}
