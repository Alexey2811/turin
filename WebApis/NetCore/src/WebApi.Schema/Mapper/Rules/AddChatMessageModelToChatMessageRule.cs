﻿using AutoMapper;
using WebApi.DAL.Entities;
using WebApi.DTO.Requests;

namespace WebApi.Schema.Mapper.Rules
{
    public class AddChatMessageModelToChatMessageRule : Profile
    {
        public AddChatMessageModelToChatMessageRule()
        {
            CreateMap<AddChatMessageModel, ChatMessages>()
               .ForMember(dest => dest.ChatId, opt => opt.MapFrom(r => r.ChatId))
               .ForMember(dest => dest.CreatorId, opt => opt.MapFrom(r => r.CreatorId))
               .ForMember(dest => dest.Message, opt => opt.MapFrom(r => r.Message));
        }
    }
}
