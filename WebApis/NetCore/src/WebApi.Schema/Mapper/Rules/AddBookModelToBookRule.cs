using System;
using AutoMapper;
using WebApi.DAL.Entities;
using WebApi.DTO.Requests;

namespace WebApi.Schema.Mapper.Rules
{
    public class AddBookModelToBookRule: Profile
    {
        public AddBookModelToBookRule()
        {
            CreateMap<AddBookModel, Books>()
                .ForMember(r => r.Author, r => r.MapFrom(t => t.Author))
                .ForMember(r => r.Description, r => r.MapFrom(t => t.Description))
                .ForMember(r => r.Name, r => r.MapFrom(t => t.Name))
                .ForMember(r => r.CreationDate, r => r.MapFrom(t => DateTime.Now));

        }
    }
}