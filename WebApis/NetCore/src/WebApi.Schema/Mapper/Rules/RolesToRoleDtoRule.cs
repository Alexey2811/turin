﻿using AutoMapper;
using WebApi.DAL.Entities;
using WebApi.DTO.Mapping;

namespace WebApi.Schema.Mapper.Rules
{
    public class RolesToRoleDtoRule : Profile
    {
        public RolesToRoleDtoRule()
        {
            CreateMap<Roles, RoleDto>()
                .ForMember(dest => dest.RoleId, opt => opt.MapFrom(r => r.RoleId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(r => r.Name));
        }
    }
}
