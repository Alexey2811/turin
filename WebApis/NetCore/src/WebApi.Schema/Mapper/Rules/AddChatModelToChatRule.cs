﻿using AutoMapper;
using WebApi.DAL.Entities;
using WebApi.DTO.Requests;

namespace WebApi.Schema.Mapper.Rules
{
    public class AddChatModelToChatRule : Profile
    {
        public AddChatModelToChatRule()
        {
            CreateMap<AddChatModel, Chats>()
               .ForMember(dest => dest.CreatorId, opt => opt.MapFrom(r => r.CreatorId))
               .ForMember(dest => dest.Name, opt => opt.MapFrom(r => r.Name));
        }
    }
}
