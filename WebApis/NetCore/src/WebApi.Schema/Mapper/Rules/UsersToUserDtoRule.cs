﻿using AutoMapper;
using System.Linq;
using WebApi.DAL.Entities;
using WebApi.DTO.Mapping;

namespace WebApi.Schema.Mapper.Rules
{
    public class UsersToUserDtoRule : Profile
    {
        public UsersToUserDtoRule()
        {
            CreateMap<Users, UserDto>().ForMember(dest => dest.UserId, opt => opt.MapFrom(r => r.UserId))
                .ForMember(dest => dest.Login, opt => opt.MapFrom(r => r.Login))
                .ForMember(dest => dest.Roles, opt => opt.MapFrom(r=>r.LinkUsersRoles.Select(link=>link.Role)));
        }
    }
}
