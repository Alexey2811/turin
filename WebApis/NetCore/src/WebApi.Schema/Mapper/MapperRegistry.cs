﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using WebApi.Schema.Mapper.Rules;

namespace WebApi.Schema.Mapper
{
    public static class MapperRegistry
    {
        public static IServiceCollection AddMapper(this IServiceCollection collection)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new UsersToUserDtoRule());
                mc.AddProfile(new RolesToRoleDtoRule());
                mc.AddProfile(new BooksToBooksDtoRule());
                mc.AddProfile(new AddUserModelToUsersRule());
                mc.AddProfile(new RoleDtoToLinkUsersRoles());
                mc.AddProfile(new BooksDtoToBooksRule());
                mc.AddProfile(new AddChatModelToChatRule());
                mc.AddProfile(new ChatMessagesToChatMessagesDtoRule());
                mc.AddProfile(new AddChatMessageModelToChatMessageRule());
                mc.AddProfile(new ChatToChatDtoRule());
                mc.AddProfile(new AddBookModelToBookRule());
            });

            var mapper = mappingConfig.CreateMapper();
            collection.AddSingleton(mapper);
            return collection;
        }
    }
}
