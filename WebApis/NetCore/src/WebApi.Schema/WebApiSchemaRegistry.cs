using Microsoft.Extensions.DependencyInjection;
using WebApi.DAL;
using WebApi.Schema.Crypto;
using WebApi.Schema.Mapper;
using WebApi.Schema.Services;

namespace WebApi.Schema
{
    public static class WebApiSchemaRegistry
    {
        public static IServiceCollection AddWebApiSchema(this IServiceCollection collection, string connectionString = default)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                collection.AddInMemoryDbContext();
            }
            else
            {
                collection.AddPostgreSqlDbContext(connectionString);
            }

            collection.AddMemoryCache();
            collection.AddMapper();
            collection.AddCrypto();
            collection.AddWebApiServices();

            return collection;
        }
    }
}