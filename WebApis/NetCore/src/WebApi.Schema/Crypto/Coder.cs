﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace WebApi.Schema.Crypto
{
    public class Coder : ICoder
    {
        public string GetCodeByValSalt(string val)
        {
            using SHA256 sha256Hash = SHA256.Create();
            var hash = GetHash(sha256Hash, val);

            return hash;
        }

        public bool Verify(string password, string hashedPassword)
        {
            using SHA256 sha256Hash = SHA256.Create();
            var hash = GetHash(sha256Hash, password);

            return VerifyHash(sha256Hash,password, hash);
        }
        private string GetHash(HashAlgorithm hashAlgorithm, string input)
        {
            var data = hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(input));
            var sBuilder = new StringBuilder();

            foreach (var t in data)
            {
                sBuilder.Append(t.ToString("x2"));
            }
            return sBuilder.ToString();
        }
        
        private bool VerifyHash(HashAlgorithm hashAlgorithm, string input, string hash)
        {
            var hashOfInput = GetHash(hashAlgorithm, input);

            var comparer = StringComparer.OrdinalIgnoreCase;

            return comparer.Compare(hashOfInput, hash) == 0;
        }
    }
}
