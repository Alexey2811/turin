﻿namespace WebApi.Schema.Crypto
{
    public interface ICoder
    {
        string GetCodeByValSalt(string val);
        bool Verify(string password, string hashedPassword);
    }
}
