﻿using Microsoft.Extensions.DependencyInjection;

namespace WebApi.Schema.Crypto
{
    public static class CryptoRegistry
    {
        public static IServiceCollection AddCrypto(this IServiceCollection collection)
        {
            collection.AddTransient<ICoder, Coder>();
            return collection;
        }
    }
}
