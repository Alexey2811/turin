﻿namespace WebApi.DTO.Requests
{
    public class RequestPaginationWithChatIdModel : RequestPaginationModel
    {
        public int ChatId { get; set; }
    }
}
