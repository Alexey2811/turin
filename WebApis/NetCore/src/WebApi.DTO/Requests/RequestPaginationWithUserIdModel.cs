﻿namespace WebApi.DTO.Requests
{
    public class RequestPaginationWithUserIdModel : RequestPaginationModel
    {
        public int UserId { get; set; }
    }
}
