﻿namespace WebApi.DTO.Requests
{
    public class UpdateChatModel
    {
        public int ChatId { get; set; }
        public string ChatName { get; set; }
    }
}
