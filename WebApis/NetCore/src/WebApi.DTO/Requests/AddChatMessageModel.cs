﻿namespace WebApi.DTO.Requests
{
    public class AddChatMessageModel
    {
        public int ChatId { get; set; }
        public int CreatorId { get; set; }
        public string Message { get; set; }
    }
}
