namespace WebApi.DTO.Requests
{
    public class AddBookModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
    }
}