﻿namespace WebApi.DTO.Requests
{
    public class UpdateUserModel
    {
        public int UserId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
