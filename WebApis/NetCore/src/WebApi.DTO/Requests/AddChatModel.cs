﻿namespace WebApi.DTO.Requests
{
    public class AddChatModel
    {
        public string Name { get; set; }
        public int CreatorId { get; set; }
    }
}
