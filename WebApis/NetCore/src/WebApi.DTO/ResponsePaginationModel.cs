﻿using System.Collections.Generic;

namespace WebApi.DTO
{
    public class ResponsePaginationModel<T> where T : class
    {
        public int Count { get; set; }
        public IEnumerable<T> Items { get; set; }
    }
}
