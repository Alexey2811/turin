﻿namespace WebApi.DTO
{
    public class RequestPaginationModel
    {
        public int Page { get; set; }
        public int Size { get; set; }
    }
}
