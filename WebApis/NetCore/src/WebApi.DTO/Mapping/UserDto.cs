﻿using System.Collections.Generic;

namespace WebApi.DTO.Mapping
{
    public class UserDto
    {
        public int UserId { get; set; }
        public string Login { get; set; }
        public IEnumerable<RoleDto> Roles { get; set; }
    }
}
