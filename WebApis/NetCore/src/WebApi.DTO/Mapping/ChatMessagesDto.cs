﻿namespace WebApi.DTO.Mapping
{
    public class ChatMessagesDto
    {
        public int ChatMessageId { get; set; }

        public int CreatorId { get; set; }
        public string CreatorName { get; set; }

        public int ChatId { get; set; }

        public string Message { get; set; }
    }
}
