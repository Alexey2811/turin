﻿using System;

namespace WebApi.DTO.Mapping
{
    public class BookDto
    {
        public int BookId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
