﻿namespace WebApi.DTO.Mapping
{
    public class ChatDto
    {
        public int ChatId { get; set; }
        public string Name { get; set; }
        public int CreatorId { get; set; }
        public string CreatorName { get; set; }
    }
}
