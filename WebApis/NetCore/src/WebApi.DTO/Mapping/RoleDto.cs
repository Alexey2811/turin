﻿namespace WebApi.DTO.Mapping
{
    public class RoleDto
    {
        public int RoleId { get; set; }
        public string Name { get; set; }
    }
}
