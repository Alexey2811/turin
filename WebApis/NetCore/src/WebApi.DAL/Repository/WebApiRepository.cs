﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DAL.Repository
{
    public class WebApiRepository<T> : IWebApiRepository<T> where T : class
    {
        private readonly WebApiContext _context;
        private readonly DbSet<T> _root;
        private bool _disposed;

        public WebApiRepository(WebApiContext context)
        {
            _context = context;
            _root = context.Set<T>();
        }

        ~WebApiRepository()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public IQueryable<T> GetQuery()
        {
            return _root.AsQueryable();
        }

        public void Add(T item)
        {
            _root.Add(item);
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Update(T item)
        {
            _root.Update(item);
        }

        public void Delete(T item)
        {
            _root.Remove(item);
        }
    }
}
