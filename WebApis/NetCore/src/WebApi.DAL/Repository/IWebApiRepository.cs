﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DAL.Repository
{
    public interface IWebApiRepository<T> : IDisposable where T : class
    {
        IQueryable<T> GetQuery();
        void Add(T item);
        Task SaveAsync();
        void Update(T item);
        void Delete(T item);
    }
}
