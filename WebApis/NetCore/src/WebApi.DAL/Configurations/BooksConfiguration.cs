﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebApi.DAL.Entities;

namespace WebApi.DAL.Configurations
{
    internal class BooksConfiguration : IEntityTypeConfiguration<Books>
    {
        public void Configure(EntityTypeBuilder<Books> builder)
        {
            builder.HasKey(r => r.BookId);

            builder.Property(r => r.Author)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(r => r.CreationDate)
                .IsRequired();

            builder.Property(r => r.Description)
                .IsRequired()
                .HasMaxLength(500);

            builder.Property(r => r.Name)
                .IsRequired()
                .HasMaxLength(100);
        }
    }
}
