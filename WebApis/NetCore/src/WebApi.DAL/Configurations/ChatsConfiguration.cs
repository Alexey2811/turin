﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebApi.DAL.Entities;

namespace WebApi.DAL.Configurations
{
    internal class ChatsConfiguration : IEntityTypeConfiguration<Chats>
    {
        public void Configure(EntityTypeBuilder<Chats> builder)
        {
            builder.HasKey(r => r.ChatId);

            builder.HasIndex(r => r.CreatorId);

            builder.HasOne(r => r.Creator)
                .WithMany(r => r.Chats)
                .HasForeignKey(r => r.CreatorId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Property(r => r.Name)
                .IsRequired()
                .HasMaxLength(250);

            builder.Property(r => r.CreatorId)
                .IsRequired();
        }
    }
}
