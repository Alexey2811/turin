﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebApi.DAL.Entities;

namespace WebApi.DAL.Configurations
{
    internal class LinkUsersRolesConfiguration : IEntityTypeConfiguration<LinkUsersRoles>
    {
        public void Configure(EntityTypeBuilder<LinkUsersRoles> builder)
        {
            builder.HasKey(r => new { r.RoleId, r.UserId });

            builder.HasOne(r => r.User)
                .WithMany(r => r.LinkUsersRoles)
                .HasForeignKey(r => r.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(r => r.Role)
                .WithMany(r => r.LinkUsersRoles)
                .HasForeignKey(r => r.RoleId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Property(r => r.UserId)
                .IsRequired();

            builder.Property(r => r.RoleId)
                .IsRequired();
        }
    }
}
