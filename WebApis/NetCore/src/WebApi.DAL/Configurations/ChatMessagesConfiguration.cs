﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebApi.DAL.Entities;

namespace WebApi.DAL.Configurations
{
    internal class ChatMessagesConfiguration : IEntityTypeConfiguration<ChatMessages>
    {
        public void Configure(EntityTypeBuilder<ChatMessages> builder)
        {
            builder.HasKey(r => r.ChatMessageId);

            builder.HasIndex(r => r.ChatId);

            builder.HasOne(r => r.Creator)
                .WithMany(r => r.ChatMessages)
                .HasForeignKey(r => r.CreatorId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(r => r.Chat)
                .WithMany(r => r.ChatMessages)
                .HasForeignKey(r => r.ChatId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Property(r => r.Message)
                .IsRequired()
                .HasMaxLength(250);

            builder.Property(r => r.CreatorId)
                .IsRequired();

            builder.Property(r => r.ChatId)
                .IsRequired();
        }
    }
}
