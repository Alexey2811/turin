﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebApi.DAL.Entities;

namespace WebApi.DAL.Configurations
{
    internal class UsersConfiguration : IEntityTypeConfiguration<Users>
    {
        public void Configure(EntityTypeBuilder<Users> builder)
        {
            builder.HasKey(r => r.UserId);

            builder
                .HasIndex(r => r.Login)
                .IsUnique();

            builder.Property(r => r.Login)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(r => r.PasswordHash)
                .IsRequired()
                .HasMaxLength(250);
        }
    }
}
