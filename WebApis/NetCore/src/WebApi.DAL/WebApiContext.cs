﻿using Microsoft.EntityFrameworkCore;
using WebApi.DAL.Configurations;
using WebApi.DAL.Entities;

namespace WebApi.DAL
{
    public class WebApiContext : DbContext
    {
        public DbSet<Users> Users { get; set; }
        public DbSet<Books> Books { get; set; }
        public DbSet<ChatMessages> ChatMessages { get; set; }
        public DbSet<Chats> Chats { get; set; }
        public DbSet<Roles> Roles { get; set; }
        public DbSet<LinkUsersRoles> LinkUsersRoles { get; set; }

        public WebApiContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .ApplyConfiguration(new BooksConfiguration())
                .ApplyConfiguration(new ChatMessagesConfiguration())
                .ApplyConfiguration(new ChatsConfiguration())
                .ApplyConfiguration(new RolesConfiguration())
                .ApplyConfiguration(new UsersConfiguration())
                .ApplyConfiguration(new LinkUsersRolesConfiguration());
        }
    }
}
