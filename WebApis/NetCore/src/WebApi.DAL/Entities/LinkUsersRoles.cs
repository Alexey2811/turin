﻿namespace WebApi.DAL.Entities
{
    public class LinkUsersRoles
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public Users User { get; set; }
        public Roles Role { get; set; }
    }
}
