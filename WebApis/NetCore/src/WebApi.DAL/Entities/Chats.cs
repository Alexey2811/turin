﻿using System.Collections.Generic;

namespace WebApi.DAL.Entities
{
    public class Chats
    {
        public int ChatId { get; set; }
        public string Name { get; set; }
        public int CreatorId { get; set; }
        public virtual Users Creator { get; set; }
        public virtual ICollection<ChatMessages> ChatMessages { get; set; } = new HashSet<ChatMessages>();
    }
}
