﻿using System.Collections.Generic;

namespace WebApi.DAL.Entities
{
    public class Users
    {
        public int UserId { get; set; }
        public string Login { get; set; }
        public string PasswordHash { get; set; }

        public ICollection<LinkUsersRoles> LinkUsersRoles { get; set; } = new HashSet<LinkUsersRoles>();
        public ICollection<Chats> Chats { get; set; } = new HashSet<Chats>();
        public ICollection<ChatMessages> ChatMessages { get; set; } = new HashSet<ChatMessages>();
    }
}
