﻿using System;

namespace WebApi.DAL.Entities
{
    public class Books
    {
        public int BookId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
