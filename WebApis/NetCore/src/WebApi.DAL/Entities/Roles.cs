﻿using System.Collections.Generic;

namespace WebApi.DAL.Entities
{
    public class Roles
    {
        public int RoleId { get; set; }
        public string Name { get; set; }
        public ICollection<LinkUsersRoles> LinkUsersRoles { get; set; } = new HashSet<LinkUsersRoles>();
    }
}
