﻿namespace WebApi.DAL.Entities
{
    public class ChatMessages
    {
        public int ChatMessageId { get; set; }

        public int CreatorId { get; set; }
        public virtual Users Creator { get; set; }

        public int ChatId { get; set; }
        public virtual Chats Chat { get; set; }

        public string Message { get; set; }
    }
}
