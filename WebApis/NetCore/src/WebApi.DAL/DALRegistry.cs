﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using WebApi.DAL.Repository;

namespace WebApi.DAL
{
    public static class DalRegistry
    {
        public static IServiceCollection AddInMemoryDbContext(this IServiceCollection collection)
        {
            collection.AddDbContext<WebApiContext>(builder => {
                builder.UseInMemoryDatabase("WebApi");
            });
            collection.AddScoped(typeof(IWebApiRepository<>), typeof(WebApiRepository<>));
            return collection;
        }
        public static IServiceCollection AddPostgreSqlDbContext(this IServiceCollection collection, 
            string connectionString)
        {
            collection.AddDbContext<WebApiContext>(builder => {
                builder.UseNpgsql(connectionString);
            });
            collection.AddScoped(typeof(IWebApiRepository<>), typeof(WebApiRepository<>));
            return collection;
        }
    }
}
