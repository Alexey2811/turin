using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using WebApi.DAL.Entities;

namespace WebApi.DAL.FakeData
{
    public class DataWriter
    {
        public static async Task WriteAsync(IServiceProvider provider,
            ILoggerFactory loggerFactory)
        {
            var logger = loggerFactory.CreateLogger<DataWriter>();

            try
            {
                logger.LogInformation("start migration");
                await using var ctx = provider.GetService<WebApiContext>();
                await ctx.Database.MigrateAsync();

                if (!ctx.Roles.Any())
                {
                    logger.LogInformation("add fake data");
                    ctx.LinkUsersRoles.Add(new LinkUsersRoles
                    {
                        User = new Users
                        {
                            Login = "turin",
                            PasswordHash = "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3"
                        },
                        Role = new Roles
                        {
                            RoleId = 1,
                            Name = "Reader"
                        }
                    });
                    ctx.Books.AddRange(new[]
                    {
                        new Books
                        {
                            Name = "name1",
                            Description = "description1",
                            CreationDate = DateTime.Now,
                            Author = "turin1"
                        },
                        new Books
                        {
                            Name = "name2",
                            Description = "description2",
                            CreationDate = DateTime.Now,
                            Author = "turin2"
                        }
                    });
                    await ctx.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                var ex = new Exception("error migrate", e);
                logger.LogError(ex, "error write");
                throw ex;
            }
        }
    }
}