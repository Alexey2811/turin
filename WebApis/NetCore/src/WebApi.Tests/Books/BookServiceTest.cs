﻿using NUnit.Framework;
using System;
using System.Threading.Tasks;
using WebApi.DTO;
using WebApi.DTO.Mapping;
using WebApi.DTO.Requests;
using WebApi.Schema.Services.Books;

namespace WebApi.Tests.Books
{
    [TestFixture]
    public class BookServiceTest
    {
        [Test]
        public async Task GetBooksTest()
        {
            var service = Helper.Services.GetService<IBookService>();


            await AddBookAsync(service);
            await AddBookAsync(service);
            await AddBookAsync(service);

            var result = await service.GetBooksAsync(new RequestPaginationModel
            {
                Page = 0,
                Size = 10
            });
            
            Assert.NotZero(result.Count);
        }

        [Test]
        public async Task UpdateBookTest()
        {
            var service = Helper.Services.GetService<IBookService>();

            var newBook = await AddBookAsync(service);

            const string newName = "testUpdate";

            var updateModel = new UpdateBookModel
            {
                BookId = newBook.BookId,
                Author = newBook.Author,
                Description = newBook.Description,
                Name = newName
            };


            await service.UpdateBookAsync(updateModel);

            var check = await service.GetBookByIdAsync(newBook.BookId);
            
            Assert.IsTrue(check.Name.Equals(newName));
        }
        
        [Test]
        public async Task DeleteBookTest()
        {
            var service = Helper.Services.GetService<IBookService>();

            var newBook = await AddBookAsync(service);
            await service.DeleteBookAsync(newBook.BookId);
            try
            {
                await service.GetBookByIdAsync(newBook.BookId);
            }
            catch
            {
                Assert.Pass();
            }
            
            Assert.Fail();
        }

        [Test]
        public async Task GetBookByIdTest()
        {
            var service = Helper.Services.GetService<IBookService>();

            var newBook = await AddBookAsync(service);

            var check = await service.GetBookByIdAsync(newBook.BookId);
            
            Assert.IsNotNull(check);
        }

        #region Helpers
        private async Task<BookDto> AddBookAsync(IBookService service)
        {
            var newBook = new AddBookModel
            {
                Description = Guid.NewGuid().ToString("D"),
                Name = Guid.NewGuid().ToString("D"),
                Author = Guid.NewGuid().ToString("D")
            };

            return await service.AddBookAsync(newBook);
        }
        #endregion
    }
}
