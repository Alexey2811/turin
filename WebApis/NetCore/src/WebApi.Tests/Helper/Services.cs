﻿using Microsoft.Extensions.DependencyInjection;
using WebApi.Schema;

namespace WebApi.Tests.Helper
{
    public static class Services
    {
        public static T GetService<T>()
        {
            var serviceProvider = new ServiceCollection()
                .AddWebApiSchema()
                .BuildServiceProvider();

            return serviceProvider.GetService<T>();
        }
    }
}
