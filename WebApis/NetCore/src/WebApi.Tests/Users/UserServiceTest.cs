﻿using NUnit.Framework;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApi.DAL.Entities;
using WebApi.DAL.Repository;
using WebApi.DTO.Mapping;
using WebApi.DTO.Requests;
using WebApi.Schema.Services.Users;

namespace WebApi.Tests.Users
{
    [TestFixture]
    public class UserServiceTest
    {
        private const string UserPassword = "123";
        private const string UserName = "turinTest";
        
        [Test]
        public async Task GetUserByLoginPassword()
        {
            var service = Helper.Services.GetService<IUserService>();
            var rolesService = Helper.Services.GetService<IWebApiRepository<Roles>>();

            await AddUserAsync(service, rolesService);

            var check = await service.GetUserByLoginPasswordAsync(new AuthorizationModel
            {
                Login = UserName,
                Password = UserPassword
            });
            
            Assert.IsNotNull(check);
        }

        [Test]
        public async Task AddUserTest()
        {
            var service = Helper.Services.GetService<IUserService>();
            var rolesService = Helper.Services.GetService<IWebApiRepository<Roles>>();

            var check = await AddUserAsync(service, rolesService);
            
            Assert.IsNotNull(check);
        }
        
        [Test]
        public async Task UpdateUserTest()
        {
            var service = Helper.Services.GetService<IUserService>();
            var rolesService = Helper.Services.GetService<IWebApiRepository<Roles>>();

            var user = await AddUserAsync(service, rolesService);

            const string newLogin = "newLogin";
            const string newPassword = "234";

            await service.UpdateUserAsync(new UpdateUserModel
            {
                Login = newLogin,
                Password = newPassword,
                UserId = user.UserId
            });

            var check = await service.GetUserByIdAsync(user.UserId);
            
            Assert.IsTrue(check.Login.Equals(newLogin));
        }

        [Test]
        public async Task GetUserByIdAsyncTest()
        {
            var service = Helper.Services.GetService<IUserService>();
            var rolesService = Helper.Services.GetService<IWebApiRepository<Roles>>();

            var user = await AddUserAsync(service, rolesService);

            var check = await service.GetUserByIdAsync(user.UserId);
            
            Assert.IsNotNull(check);
        }

        [Test]
        public async Task IsExistLoginTest()
        {
            var service = Helper.Services.GetService<IUserService>();
            var rolesService = Helper.Services.GetService<IWebApiRepository<Roles>>();

            var newUser = await AddUserAsync(service, rolesService);

            var check = await service.IsExistLogin(newUser.Login);

            Assert.NotNull(check);
        }

        #region Helpers

        private async Task<UserDto> AddUserAsync(IUserService service, IWebApiRepository<Roles> repository)
        {
            var newRole = new Roles
            {
                RoleId = 1,
                Name = "Reader"
            };
            
            var role = await repository.GetQuery().FirstOrDefaultAsync(r=>r.RoleId==newRole.RoleId);
            if (role == default)
            {
                repository.Add(newRole);
                await repository.SaveAsync();
            }
            
            var newModel = new AddUserModel
            {
                Login = UserName,
                Password = UserPassword
            };
            return await service.AddUserAsync(newModel);
        }
        #endregion
    }
}
