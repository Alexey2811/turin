﻿using Microsoft.Extensions.Caching.Memory;
using NUnit.Framework;
using WebApi.DTO.Mapping;
using WebApi.Schema.Crypto;
using WebApi.Schema.Services.Users.Memory;

namespace WebApi.Tests.Users
{
    [TestFixture]
    public class UserMemoryTest
    {
        [Test]
        public void AddUserInMemory()
        {
            var memory= Helper.Services.GetService<IMemoryCache>();
            var coder = Helper.Services.GetService<ICoder>();

            var testClass = new UserMemory(memory, coder);

            const string testLogin = "test2login";
            var token = AddUserInMemory(testClass, testLogin, 2);

            var controlUser = testClass.GetUserByToken(token);

            Assert.IsTrue(controlUser.Login.Equals(testLogin));
        }
        [Test]
        public void RemoveUserFromMemory()
        {
            try
            {
                var memory = Helper.Services.GetService<IMemoryCache>();
                var coder = Helper.Services.GetService<ICoder>();

                var testClass = new UserMemory(memory, coder);

                var token = AddUserInMemory(testClass, "login1", 1);

                
                testClass.RemoveUserByToken(token);

                testClass.GetUserByToken(token);
                Assert.Fail();
            }
            catch
            {
                Assert.Pass();
            }
        }

        #region Helpers
        private static string AddUserInMemory(IUserMemory userMemory, string login, int userId)
        {
            var user = new UserDto { Login = login, UserId = userId };

            var token = userMemory.AddUserInMemory(user);

            return token;
        }
        #endregion
    }
}
