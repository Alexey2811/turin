﻿using System;
using NUnit.Framework;
using System.Threading.Tasks;
using WebApi.DTO.Mapping;
using WebApi.DTO.Requests;
using WebApi.Schema.Services.Chat.Messages;

namespace WebApi.Tests.Chat
{
    [TestFixture]
    public class ChatMessagesServiceTest
    {
        [Test]
        public async Task GetMessagesAsyncTest()
        {
            var service = Helper.Services.GetService<IChatMessagesService>();

            await AddChatMessagesAsync(service, 1, 1);
            await AddChatMessagesAsync(service, 1, 1);
            await AddChatMessagesAsync(service, 1, 1);

            var check = await service.GetMessagesAsync(new RequestPaginationWithChatIdModel
            {
                ChatId = 1,
                Page = 0,
                Size = 10
            });
            
            Assert.NotZero(check.Count);
        }

        [Test]
        public async Task AddChatMessagesAsyncTest()
        {
            
            var service = Helper.Services.GetService<IChatMessagesService>();

            var newModel = await AddChatMessagesAsync(service, 1, 1);
            
            Assert.IsNotNull(newModel);
        }

        [Test]
        public async Task DeleteMessageTest()
        {
            var service = Helper.Services.GetService<IChatMessagesService>();

            var newModel = await AddChatMessagesAsync(service, 1, 1);

            await service.DeleteMessageAsync(newModel.ChatMessageId);

            try
            {
                await service.GetMesssageByIdAsync(newModel.ChatMessageId);
            }
            catch 
            {
                Assert.Pass();
            }
            
            Assert.Fail();
        }

        [Test]
        public async Task GetChatMessageByIdAsyncTest()
        {
            var service = Helper.Services.GetService<IChatMessagesService>();

            var newModel = await AddChatMessagesAsync(service, 1, 1);

            var check = await service.GetMesssageByIdAsync(newModel.ChatMessageId);
            
            Assert.IsNotNull(check);
        }

        #region Helper

        private async Task<ChatMessagesDto> AddChatMessagesAsync(IChatMessagesService service, int chatId, int creatorId)
        {
            var newChatModel = new AddChatMessageModel
            {
                ChatId = chatId,
                CreatorId = creatorId,
                Message = Guid.NewGuid().ToString("D")
            };

            return await service.AddChatMessagesAsync(newChatModel);
        }

        #endregion
    }
}
