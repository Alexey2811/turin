﻿using System;
using NUnit.Framework;
using System.Threading.Tasks;
using WebApi.DTO;
using WebApi.DTO.Mapping;
using WebApi.DTO.Requests;
using WebApi.Schema.Services.Chat;

namespace WebApi.Tests.Chat
{
    [TestFixture]
    public class ChatServiceTest
    {
        [Test]
        public async Task AddChatTest()
        {
            var service = Helper.Services.GetService<IChatService>();

            var check = await AddChatAsync(service, 1);
            
            Assert.IsNotNull(check);
        }

        [Test]
        public async Task GetMyChatsTest()
        {
            var service = Helper.Services.GetService<IChatService>();
            
            await AddChatAsync(service, 2);
            await AddChatAsync(service, 2);
            await AddChatAsync(service, 2);

            var check = await service.GetMyChatsAsync(new RequestPaginationWithUserIdModel
            {
                UserId = 2,
                Page = 0,
                Size = 10
            });
            
            Assert.NotZero(check.Count);

        }

        [Test]
        public async Task GetAllChatsTest()
        {
            var service = Helper.Services.GetService<IChatService>();
            
            await AddChatAsync(service, 2);
            await AddChatAsync(service, 2);
            await AddChatAsync(service, 2);

            var check = await service.GetAllChatsAsync(new RequestPaginationModel
            {
                Page = 0,
                Size = 10
            });
            
            Assert.NotZero(check.Count);
        }

        [Test]
        public async Task DeleteChatTest()
        {
            var service = Helper.Services.GetService<IChatService>();
            
            var newChat = await AddChatAsync(service, 2);

            await service.DeleteChatAsync(newChat.ChatId);

            try
            {
                await service.GetChatByIdAsync(newChat.ChatId);
            }
            catch 
            {
                Assert.Pass();
            }
            Assert.Fail();
        }

        [Test]
        public async Task UpdateChatTest()
        {
            var service = Helper.Services.GetService<IChatService>();

            const string newName = "tepUpdate";

            var newChat = await AddChatAsync(service, 5);

            var updateModel = new UpdateChatModel
            {
                ChatId = newChat.ChatId,
                ChatName = newName
            };

            await service.UpdateChatAsync(updateModel);

            var check = await service.GetChatByIdAsync(newChat.ChatId);
            
            Assert.IsTrue(check.Name.Equals(newName));
        }

        [Test]
        public async Task GetChatByIdAsyncTest()
        {
            var service = Helper.Services.GetService<IChatService>();
            
            var newChat = await AddChatAsync(service, 2);

            var check = await service.GetChatByIdAsync(newChat.ChatId);
            
            Assert.IsNotNull(check);
        }

        #region Helpers
        private async Task<ChatDto> AddChatAsync(IChatService service, int creatorId)
        {
            if (creatorId <= 0) throw new ArgumentOutOfRangeException(nameof(creatorId));
            var newChat = new AddChatModel
            {
                Name = Guid.NewGuid().ToString("D"),
                CreatorId = creatorId,
            };
            return await service.AddChatAsync(newChat);
        }
        
        #endregion
    }
}
