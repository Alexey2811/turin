﻿using NUnit.Framework;
using WebApi.Schema.Crypto;

namespace WebApi.Tests.Crypto
{
    [TestFixture]
    public class CoderTest
    {
        [Test]
        public void TestMd5Crypto()
        {
            const string testVal = "testVal";

            var coder = new Coder();

            var result = coder.GetCodeByValSalt(testVal);

            Assert.IsTrue(coder.Verify(testVal, result));
        }
    }
}
