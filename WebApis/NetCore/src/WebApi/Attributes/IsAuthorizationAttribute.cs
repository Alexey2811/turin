using Microsoft.AspNetCore.Mvc;
using WebApi.Filters;

namespace WebApi.Attributes
{
    public class IsAuthorizationAttribute : TypeFilterAttribute
    {
        public IsAuthorizationAttribute() : base(typeof(IsAuthorizationFilter))
        {
            
        }
        
    }
}