using Microsoft.AspNetCore.Mvc;
using WebApi.Filters;

namespace WebApi.Attributes
{
    public class CanDeleteChatMessageAttribute : TypeFilterAttribute
    {
        public CanDeleteChatMessageAttribute() : base(typeof(CanDeleteChatMessageFilter))
        {
            
        }
        
    }
}