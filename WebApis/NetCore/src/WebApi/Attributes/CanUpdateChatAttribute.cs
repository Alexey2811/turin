using Microsoft.AspNetCore.Mvc;
using WebApi.Filters;

namespace WebApi.Attributes
{
    public class CanUpdateChatAttribute : TypeFilterAttribute
    {
        public CanUpdateChatAttribute() : base(typeof(CanUpdateChatFilter))
        {
            
        }
        
    }
}