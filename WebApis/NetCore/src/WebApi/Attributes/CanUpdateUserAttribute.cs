using Microsoft.AspNetCore.Mvc;
using WebApi.Filters;

namespace WebApi.Attributes
{
    public class CanUpdateUserAttribute : TypeFilterAttribute
    {
        public CanUpdateUserAttribute() : base(typeof(CanUpdateUserFilter))
        {
            
        }
        
    }
}