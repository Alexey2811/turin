using Microsoft.AspNetCore.Mvc;
using WebApi.Filters;

namespace WebApi.Attributes
{
    public class CanDeleteChatAttribute : TypeFilterAttribute
    {
        public CanDeleteChatAttribute() : base(typeof(CanDeleteChatFilter))
        {
            
        }
        
    }
}