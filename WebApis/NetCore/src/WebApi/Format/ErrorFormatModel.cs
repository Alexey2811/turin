﻿namespace WebApi.Format
{
    public class ErrorFormatModel
    {
        public int ErrorCode { get; set; }
        public string Message { get; set; }
    }
}
