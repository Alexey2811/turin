﻿using FluentValidation;
using WebApi.DTO.Requests;

namespace WebApi.Validators.Rules
{
    public class AddChatModelValidator : AbstractValidator<AddChatModel>
    {
        public AddChatModelValidator()
        {
            RuleFor(r => r.CreatorId).InclusiveBetween(1, int.MaxValue);

            RuleFor(r => r.Name).MaximumLength(250);
            RuleFor(r => r.Name).NotEmpty();
        }
    }
}
