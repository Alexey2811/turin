﻿using FluentValidation;
using WebApi.DTO.Requests;

namespace WebApi.Validators.Rules
{
    public class RequestPaginationWithUserIdModelValidator : AbstractValidator<RequestPaginationWithUserIdModel>
    {
        public RequestPaginationWithUserIdModelValidator()
        {
            RuleFor(r => r.UserId).InclusiveBetween(1, int.MaxValue);

            RuleFor(r => r.Page).InclusiveBetween(0, int.MaxValue);
            RuleFor(r => r.Size).InclusiveBetween(1, 300);
        }
    }
}
