﻿using FluentValidation;
using WebApi.DTO.Requests;

namespace WebApi.Validators.Rules
{
    public class UpdateChatModelValidator : AbstractValidator<UpdateChatModel>
    {
        public UpdateChatModelValidator()
        {
            RuleFor(r => r.ChatId).InclusiveBetween(1, int.MaxValue);

            RuleFor(r => r.ChatName).MaximumLength(100);
            RuleFor(r => r.ChatName).NotEmpty();
        }
    }
}
