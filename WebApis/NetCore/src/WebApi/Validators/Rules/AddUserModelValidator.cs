﻿using FluentValidation;
using WebApi.DTO.Requests;

namespace WebApi.Validators.Rules
{
    public class AddUserModelValidator : AbstractValidator<AddUserModel>
    {
        public AddUserModelValidator()
        {
            RuleFor(r => r.Login).MaximumLength(100);
            RuleFor(r => r.Login).NotEmpty();

            RuleFor(r => r.Password).MaximumLength(50);
            RuleFor(r => r.Password).NotEmpty();
        }
    }
}
