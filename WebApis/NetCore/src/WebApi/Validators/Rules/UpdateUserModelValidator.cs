﻿using FluentValidation;
using WebApi.DTO.Requests;

namespace WebApi.Validators.Rules
{
    public class UpdateUserModelValidator : AbstractValidator<UpdateUserModel>
    {
        public UpdateUserModelValidator()
        {
            RuleFor(r => r.Login).MaximumLength(100);
            RuleFor(r => r.Login).NotEmpty();

            RuleFor(r => r.Password).MaximumLength(100);
            RuleFor(r => r.Password).NotEmpty();

            RuleFor(r => r.UserId).InclusiveBetween(1, int.MaxValue);
        }
    }
}
