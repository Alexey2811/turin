﻿using FluentValidation;
using WebApi.DTO;

namespace WebApi.Validators.Rules
{
    public class RequestPaginationModelValidator: AbstractValidator<RequestPaginationModel>
    {
        public RequestPaginationModelValidator()
        {
            RuleFor(r => r.Page).InclusiveBetween(0, int.MaxValue);
            RuleFor(r => r.Size).InclusiveBetween(1, 300);
        }
    }
}
