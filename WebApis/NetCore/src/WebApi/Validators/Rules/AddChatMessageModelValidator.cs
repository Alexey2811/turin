﻿using FluentValidation;
using WebApi.DTO.Requests;

namespace WebApi.Validators.Rules
{
    public class AddChatMessageModelValidator : AbstractValidator<AddChatMessageModel>
    {
        public AddChatMessageModelValidator()
        {
            RuleFor(r => r.ChatId).InclusiveBetween(1, int.MaxValue);
            RuleFor(r => r.CreatorId).InclusiveBetween(1, int.MaxValue);
            RuleFor(r => r.Message).MaximumLength(250);
        }
    }
}
