﻿using FluentValidation;
using WebApi.DTO.Requests;

namespace WebApi.Validators.Rules
{
    public class AddBookModelValidator : AbstractValidator<AddBookModel>
    {
        public AddBookModelValidator()
        {
            RuleFor(r => r.Author).MaximumLength(100);
            RuleFor(r => r.Author).NotEmpty();

            RuleFor(r => r.Description).MaximumLength(500);
            RuleFor(r => r.Description).NotEmpty();

            RuleFor(r => r.Name).MaximumLength(100);
            RuleFor(r => r.Name).NotEmpty();
        }
    }
}
