﻿using FluentValidation;
using WebApi.DTO.Requests;

namespace WebApi.Validators.Rules
{
    public class UpdateBookModelValidator : AbstractValidator<UpdateBookModel>
    {
        public UpdateBookModelValidator()
        {
            RuleFor(r => r.Author).MaximumLength(100);
            RuleFor(r => r.Author).NotEmpty();

            RuleFor(r => r.Description).MaximumLength(500);
            RuleFor(r => r.Description).NotEmpty();

            RuleFor(r => r.Name).MaximumLength(100);
            RuleFor(r => r.Name).NotEmpty();

            RuleFor(r => r.BookId).InclusiveBetween(1, int.MaxValue);
        }
    }
}
