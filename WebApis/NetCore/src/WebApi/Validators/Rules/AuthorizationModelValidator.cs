﻿using FluentValidation;
using WebApi.DTO.Requests;

namespace WebApi.Validators.Rules
{
    public class AuthorizationModelValidator : AbstractValidator<AuthorizationModel>
    {
        public AuthorizationModelValidator()
        {
            RuleFor(r => r.Login).MaximumLength(100);
            RuleFor(r => r.Login).NotEmpty();

            RuleFor(r => r.Password).MaximumLength(100);
            RuleFor(r => r.Password).NotEmpty();
        }
    }
}
