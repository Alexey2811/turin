﻿using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;
using WebApi.Validators.Rules;

namespace WebApi.Validators
{
    internal static class ValidatorRegistry
    {
        public static IMvcBuilder AddFluentValidators(this IMvcBuilder builder)
        {
            builder.AddFluentValidation(fv => {
                fv.RegisterValidatorsFromAssemblyContaining<RequestPaginationModelValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<AddChatMessageModelValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<AddChatModelValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<AddUserModelValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<AuthorizationModelValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<RequestPaginationWithChatIdModelValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<RequestPaginationWithUserIdModelValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<UpdateChatModelValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<UpdateUserModelValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<AddBookModelValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<UpdateBookModelValidator>();
            });
            return builder;
        }
    }
}
