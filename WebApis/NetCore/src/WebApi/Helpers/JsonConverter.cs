﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace WebApi.Helpers
{
    public static class JsonConverter
    {
        public static string Convert<T>(T obj)
        {
            var serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            var result = JsonConvert.SerializeObject(obj, serializerSettings);

            return result;
        }
    }
}
