﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApi.Attributes;
using WebApi.DTO;
using WebApi.DTO.Mapping;
using WebApi.DTO.Requests;
using WebApi.Schema.Services.Users;
using WebApi.Schema.Services.Users.Memory;

namespace WebApi.Controllers
{
    [Route("api/authorization")]
    [ApiController]
    public class AuthorizationController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IUserMemory _userMemory;
        private readonly ILogger _logger;

        public AuthorizationController(IUserService userService, 
            IUserMemory userMemory, 
            ILoggerFactory loggerFactory)
        {
            _userService = userService;
            _userMemory = userMemory;
            _logger = loggerFactory.CreateLogger<AuthorizationController>();
        }

        [HttpPost]
        [Route("authorize")]
        public async Task<TokenModel> Authorize([FromBody]AuthorizationModel model)
        {
            try
            {
                var user = await _userService.GetUserByLoginPasswordAsync(model);

                var token = _userMemory.AddUserInMemory(user);

                return new TokenModel {Token = token};
            }
            catch(Exception e)
            {
                var ex = new Exception("authorize Error", e);

                _logger.LogError(ex, $"error in Authorize {model.Login}");

                throw ex;
            }
        }
        [HttpGet]
        [Route("signOut")]
        [IsAuthorization]
        public void SignOut([FromQuery] string token)
        {
            try
            {
                _userMemory.RemoveUserByToken(token); 
            }
            catch(Exception e)
            {
                var ex = new Exception("signOut Error", e);

                _logger.LogError(ex, "error in SignOut");

                throw ex;
            }
        }

        [HttpGet]
        [Route("getUserByToken")]
        public UserDto GetUserByToken([FromQuery] string tokenId)
        {
            try
            {
                return _userMemory.GetUserByToken(tokenId);
            }
            catch (Exception e)
            {
                var newException=new Exception("user is not found", e);
                throw newException;
            }
        }
    }
}