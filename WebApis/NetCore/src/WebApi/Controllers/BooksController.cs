﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApi.Attributes;
using WebApi.DTO;
using WebApi.DTO.Mapping;
using WebApi.DTO.Requests;
using WebApi.Schema.Services.Books;

namespace WebApi.Controllers
{
    [Route("api/books")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly IBookService _bookService;
        private readonly ILogger _logger;

        public BooksController(IBookService bookService,
            ILoggerFactory loggerFactory)
        {
            _bookService = bookService;
            _logger = loggerFactory.CreateLogger<BooksController>();
        }

        [HttpPost]
        [Route("getBooks")]
        [IsAuthorization]
        public async Task<ResponsePaginationModel<BookDto>> GetBooksAsync([FromBody]RequestPaginationModel model)
        {
            try
            {
                return await _bookService.GetBooksAsync(model);
            }
            catch (Exception e)
            {
                var ex = new Exception("getBooks Error", e);

                _logger.LogError(ex, "error in GetBooksAsync");

                throw ex;
            }
        }

        [HttpPut]
        [Route("addBook")]
        [IsAuthorization]
        public async Task<BookDto> AddBookAsync([FromBody] AddBookModel model)
        {
            try
            {
                return await _bookService.AddBookAsync(model);
            }
            catch (Exception e)
            {
                var ex = new Exception("addBook Error", e);

                _logger.LogError(ex, "error in AddBookAsync");

                throw ex;
            }
        }

        [HttpPost]
        [Route("updateBook")]
        [IsAuthorization]
        public async Task UpdateBookAsync([FromBody] UpdateBookModel model)
        {
            try
            {
                await _bookService.UpdateBookAsync(model);
            }
            catch (Exception e)
            {
                var ex = new Exception("updateBook Error", e);

                _logger.LogError(ex, "error in UpdateBookAsync");

                throw ex;
            }
        }

        [HttpDelete]
        [Route("deleteBook")]
        [IsAuthorization]
        public async Task DeleteBookAsync([FromQuery] int bookId)
        {
            try
            {
                await _bookService.DeleteBookAsync(bookId);
            }
            catch (Exception e)
            {
                var ex = new Exception("deleteBook Error", e);

                _logger.LogError(ex, "error in DeleteBookAsync");

                throw ex;
            }
        }

        [HttpGet]
        [Route("getBookById")]
        [IsAuthorization]
        public async Task<BookDto> GetBookById([FromQuery] int bookId)
        {
            try
            {
                return await _bookService.GetBookByIdAsync(bookId);
            }
            catch (Exception e)
            {
                var ex = new Exception("book not found", e);
                throw ex;
            }
        }
    }
}