﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApi.Attributes;
using WebApi.DTO;
using WebApi.DTO.Mapping;
using WebApi.DTO.Requests;
using WebApi.Hubs;
using WebApi.Schema.Services.Chat;
using WebApi.Schema.Services.Chat.Messages;

namespace WebApi.Controllers
{
    [Route("api/chatMessages")]
    [ApiController]
    public class ChatMessagesController : ControllerBase
    {
        private readonly IChatMessagesService _chatMessagesService;
        private readonly IChatService _chatService;
        private readonly ChatHub _hub;
        private readonly ILogger _logger;

        public ChatMessagesController(IChatMessagesService chatMessagesService,
            IChatService chatService,
            ILoggerFactory loggerFactory,
            ChatHub hub)
        {
            _chatMessagesService = chatMessagesService;
            _chatService = chatService;
            _hub = hub;
            _logger = loggerFactory.CreateLogger<ChatMessagesController>();
        }

        [HttpPost]
        [Route("getMessages")]
        [IsAuthorization]
        public async Task<ResponsePaginationModel<ChatMessagesDto>> GetMessagesAsync(RequestPaginationWithChatIdModel request)
        {
            try
            {
                return await _chatMessagesService.GetMessagesAsync(request);
            }
            catch (Exception e)
            {
                var ex = new Exception("getMessages Error", e);

                _logger.LogError(ex, "error in GetMessagesAsync");

                throw ex;
            }
        }

        [HttpPut]
        [Route("addChatMessages")]
        [IsAuthorization]
        public async Task<ChatMessagesDto> AddChatMessagesAsync(AddChatMessageModel model)
        {
            try
            {
                var message= await _chatMessagesService.AddChatMessagesAsync(model);

                await _hub.AddMessageAsync(message);
                
                return message;
            }
            catch (Exception e)
            {
                var ex = new Exception("addChatMessages Error", e);

                _logger.LogError(ex, "error in AddChatMessagesAsync");

                throw ex;
            }
        }

        [HttpDelete]
        [Route("deleteMessage")]
        [CanDeleteChatMessage]
        public async Task DeleteMessageAsync([FromQuery]int messageId)
        {
            try
            {
                var message = await _chatMessagesService.GetMesssageByIdAsync(messageId);

                var chat = await _chatService.GetChatByIdAsync(message.ChatId);
                
                await _chatMessagesService.DeleteMessageAsync(messageId);

                await _hub.DeleteMessage(messageId, chat.ChatId);
            }
            catch (Exception e)
            {
                var ex = new Exception("deleteMessage Error", e);

                _logger.LogError(ex, "error in DeleteMessageAsync");

                throw ex;
            }
        }
    }
}