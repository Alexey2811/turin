﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApi.Attributes;
using WebApi.DTO;
using WebApi.DTO.Mapping;
using WebApi.DTO.Requests;
using WebApi.Schema.Services.Users;
using WebApi.Schema.Services.Users.Memory;

namespace WebApi.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IUserMemory _userMemory;
        private readonly ILogger _logger;

        public UserController(IUserService userService,
            ILoggerFactory loggerFactory,
            IUserMemory userMemory)
        {
            _userService = userService;
            _userMemory = userMemory;
            _logger = loggerFactory.CreateLogger<UserController>();
        }

        [HttpPut]
        [Route("addUser")]
        public async Task<UserDto> AddUserAsync([FromBody] AddUserModel model)
        {
            try
            {
                return await _userService.AddUserAsync(model);
            }
            catch (Exception e)
            {
                var ex = new Exception("addUser Error", e);

                _logger.LogError(ex, "error in AddUserAsync");

                throw ex;
            }
        }

        [HttpPost]
        [Route("updateUser")]
        [CanUpdateUser]
        public async Task<TokenModel> UpdateUserAsync(UpdateUserModel model)
        {
            try
            {
                var token = HttpContext.Request?.Headers["token"];

                var user = _userMemory.GetUserByToken(token);

                await _userService.UpdateUserAsync(model);

                user.Login = model.Login;

                _userMemory.RemoveUserByToken(token);
                
                var newToken = _userMemory.AddUserInMemory(user);

                return new TokenModel {Token = newToken};
            }
            catch (Exception e)
            {
                var ex = new Exception("updateUser Error", e);

                _logger.LogError(ex, "error in UpdateUserAsync");

                throw ex;
            }
        }
        
        [HttpGet]
        [IsAuthorization]
        [Route("getUserById")]
        public async Task<UserDto> GetUserById([FromQuery] int userId)
        {
            try
            {
                return await _userService.GetUserByIdAsync(userId);
            }
            catch (Exception e)
            {
                var ex = new Exception("user is not found", e);
                throw ex;
            }
        }

        [HttpGet]
        [Route("isExistUser")]
        public async Task<bool> IsExistUser([FromQuery] string login)
        {
            try
            {
                return await _userService.IsExistLogin(login);
            }
            catch (Exception e)
            {
                var ex = new Exception("something wrong", e);
                throw ex;
            }
        }
    }
}