﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApi.Attributes;
using WebApi.DTO;
using WebApi.DTO.Mapping;
using WebApi.DTO.Requests;
using WebApi.Hubs;
using WebApi.Schema.Services.Chat;

namespace WebApi.Controllers
{
    [Route("api/chats")]
    [ApiController]
    public class ChatController : ControllerBase
    {
        private readonly IChatService _chatService;
        private readonly ChatHub _hub;
        private readonly ILogger _logger;

        public ChatController(IChatService chatService,
            ILoggerFactory loggerFactory,
            ChatHub hub)
        {
            _chatService = chatService;
            _hub = hub;
            _logger = loggerFactory.CreateLogger<ChatController>();
        }

        [HttpPost]
        [Route("getMyChats")]
        [IsAuthorization]
        public async Task<ResponsePaginationModel<ChatDto>> GetMyChatsAsync([FromBody] RequestPaginationWithUserIdModel request)
        {
            try
            {
                return await _chatService.GetMyChatsAsync(request);
            }
            catch (Exception e)
            {
                var ex = new Exception("getMyChats Error", e);

                _logger.LogError(ex, "error in GetMyChatsAsync");

                throw ex;
            }
        }

        [HttpPost]
        [Route("getAllChats")]
        [IsAuthorization]
        public async Task<ResponsePaginationModel<ChatDto>> GetAllChatsAsync([FromBody] RequestPaginationModel request)
        {
            try
            {
                return await _chatService.GetAllChatsAsync(request);
            }
            catch (Exception e)
            {
                var ex = new Exception("getAllChats Error", e);

                _logger.LogError(ex, "error in GetAllChatsAsync");

                throw ex;
            }
        }

        [HttpDelete]
        [Route("deleteChat")]
        [CanDeleteChat]
        public async Task DeleteChatAsync([FromQuery] int chatId)
        {
            try
            {
                await _chatService.DeleteChatAsync(chatId);
                await _hub.DeleteChatAsync(chatId);
            }
            catch (Exception e)
            {
                var ex = new Exception("deleteChat Error", e);

                _logger.LogError(ex, "error in DeleteChatAsync");

                throw ex;
            }
        }

        [HttpPut]
        [Route("addChat")]
        [IsAuthorization]
        public async Task<ChatDto> AddChatAsync([FromBody] AddChatModel model)
        {
            try
            {
                return await _chatService.AddChatAsync(model);
            }
            catch (Exception e)
            {
                var ex = new Exception("addChat Error", e);

                _logger.LogError(ex, "error in AddChatAsync");

                throw ex;
            }
        }

        [HttpPost]
        [Route("updateChat")]
        [CanUpdateChat]
        public async Task UpdateChatAsync([FromBody] UpdateChatModel model)
        {
            try
            {
                await _chatService.UpdateChatAsync(model);
            }
            catch (Exception e)
            {
                var ex = new Exception("updateChat Error", e);

                _logger.LogError(ex, "error in UpdateChatAsync");

                throw ex;
            }
        }
        
        [HttpGet]
        [Route("getChatById")]
        public async Task<ChatDto> GetChatById([FromQuery] int chatId)
        {
            try
            {
                return await _chatService.GetChatByIdAsync(chatId);
            }
            catch (Exception e)
            {
                var ex = new Exception("chat is not found", e);
                throw ex;
            }
        }
    }
}