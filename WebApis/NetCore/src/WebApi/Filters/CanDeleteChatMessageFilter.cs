using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WebApi.Schema.Services.Chat.Messages;

namespace WebApi.Filters
{
    public class CanDeleteChatMessageFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var logger = context.HttpContext.RequestServices.GetService<ILoggerFactory>()
                .CreateLogger<CanDeleteChatMessageFilter>();
            try
            {
                var chatService = context.HttpContext.RequestServices.GetService<IChatMessagesService>();

                var user = Utils.GetUserByContext(context.HttpContext);

                var chatMessageId = Convert.ToInt32(context.HttpContext.Request.Query["messageId"]);

                var chatMessage = await chatService.GetMesssageByIdAsync(chatMessageId);

                if (user.UserId != chatMessage.CreatorId)
                    throw new Exception($"user is cheater {user.Login}");
            }
            catch (Exception e)
            {
                var ex = new Exception("error in CanDeleteChatMessageFilter", e);
                logger.LogError(ex, "error in CanDeleteChatMessageFilter");
                throw ex;
            }

            await next();
        }
    }
}