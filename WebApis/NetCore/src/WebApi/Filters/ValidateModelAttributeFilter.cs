﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Filters
{
    public class ValidateModelAttributeFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (!context.ModelState.IsValid)
            {
                var stringBuilder = new StringBuilder();

                foreach (var state in context.ModelState.Values)
                {
                    foreach (var error in state.Errors)
                    {
                        stringBuilder.Append($" {error.ErrorMessage}");
                    }
                }

                var ex = new Exception(stringBuilder.ToString());
                throw ex;
            }

            await next();
        }
    }
}
