using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WebApi.DTO.Requests;

namespace WebApi.Filters
{
    public class CanUpdateUserFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var logger = context.HttpContext.RequestServices.GetService<ILoggerFactory>()
                .CreateLogger<CanUpdateUserFilter>();
            try
            {
                var user = Utils.GetUserByContext(context.HttpContext);

                if (context.ActionArguments["model"] is UpdateUserModel model)
                {

                    if (user.UserId != model.UserId)
                        throw new Exception($"user is cheater {user.Login}");
                }
            }
            catch (Exception e)
            {
                var ex = new Exception("error in CanUpdateUserFilter", e);
                logger.LogError(ex, "error in CanUpdateUserFilter");
                throw ex;
            }

            await next();
        }
    }
}