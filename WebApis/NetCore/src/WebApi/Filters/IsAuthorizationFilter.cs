using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace WebApi.Filters
{
    public class IsAuthorizationFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var logger = context.HttpContext.RequestServices.GetService<ILoggerFactory>()
                .CreateLogger<IsAuthorizationFilter>();
            try
            {
                Utils.GetUserByContext(context.HttpContext);
            }
            catch (Exception e)
            {
                var ex = new Exception("error in IsAuthorizationFilter", e);
                logger.LogError(ex, "error in IsAuthorizationFilter");
                throw ex;
            }
            await next();
        }
    }
}