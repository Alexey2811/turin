using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WebApi.DTO.Mapping;
using WebApi.DTO.Requests;
using WebApi.Schema.Services.Chat;

namespace WebApi.Filters
{
    public class CanUpdateChatFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var logger = context.HttpContext.RequestServices.GetService<ILoggerFactory>()
                .CreateLogger<CanUpdateChatFilter>();
            try
            {
                var chatService = context.HttpContext.RequestServices.GetService<IChatService>();

                var user = Utils.GetUserByContext(context.HttpContext);

                if (context.ActionArguments["model"] is UpdateChatModel model)
                {
                    var chat = await chatService.GetChatByIdAsync(model.ChatId);

                    if (user.UserId != chat.CreatorId)
                        throw new Exception($"user is cheater {user.Login}");
                }
            }
            catch (Exception e)
            {
                var ex = new Exception("error in CanUpdateChatFilter", e);
                logger.LogError(ex, "error in CanUpdateChatFilter");
                throw ex;
            }

            await next();
        }
    }
}