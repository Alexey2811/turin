using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WebApi.Schema.Services.Chat;

namespace WebApi.Filters
{
    public class CanDeleteChatFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var logger = context.HttpContext.RequestServices.GetService<ILoggerFactory>()
                .CreateLogger<CanDeleteChatFilter>();
            try
            {
                var chatService = context.HttpContext.RequestServices.GetService<IChatService>();

                var user = Utils.GetUserByContext(context.HttpContext);
                var chatId = Convert.ToInt32(context.HttpContext.Request.Query["chatId"]);

                var chat = await chatService.GetChatByIdAsync(chatId);

                if (user.UserId != chat.CreatorId)
                    throw new Exception($"user is cheater {user.Login}");
            }
            catch (Exception e)
            {
                var ex = new Exception("error in CanDeleteChatFilter", e);
                logger.LogError(ex, "error in CanDeleteChatFilter");
                throw ex;
            }

            await next();
        }
    }
}