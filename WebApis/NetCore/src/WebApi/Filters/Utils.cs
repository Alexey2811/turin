using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using WebApi.DTO.Mapping;
using WebApi.Schema.Services.Users.Memory;
using Microsoft.Extensions.DependencyInjection;

namespace WebApi.Filters
{
    public static class Utils
    {
        public static UserDto GetUserByContext(HttpContext context)
        {
            var memory = context.RequestServices.GetService<IUserMemory>();
                
            var token = context.Request?.Headers["token"];

            if (string.IsNullOrEmpty(token))
                throw new Exception("user is not Authorized");

            var user = memory.GetUserByToken(token);

            if (user == default)
                throw new Exception("user is not Authorized");

            return user;
        }
    }
}