using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using WebApi.Filters;
using WebApi.Hubs;
using WebApi.Middleware;
using WebApi.Schema;
using WebApi.Schema.Services;
using WebApi.Validators;

namespace WebApi
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }
        private const string MyAllowSpecificOrigins = "myCors";

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                    builder =>
                    {
                        builder
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowCredentials()
                            .WithOrigins("http://localhost:4300");
                    });
            });
            services.AddSignalR();
            services
                .AddControllers(opt =>
                {
                    opt.Filters.Add(typeof(ValidateModelAttributeFilter));
                }).SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
                .ConfigureApiBehaviorOptions(opt => {
                    opt.SuppressModelStateInvalidFilter = true;
                })
                .AddNewtonsoftJson(opt =>
                {
                    opt.SerializerSettings.ContractResolver =
                        new CamelCasePropertyNamesContractResolver();
                })
                .AddFluentValidators();
            services.AddWebApiSchema(Configuration["ConnectionStrings:My"]);
            
            services.AddSingleton<ChatHub>();
        }
        public void Configure(IApplicationBuilder app)
        {
            //app.UseAuthentication();
            app.UseCors(MyAllowSpecificOrigins);
            
            //custom error format
            app.UseMiddleware(typeof(ErrorHandlingMiddleware));

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<ChatHub>("/chatHub");
            });
            
            UpdateDatabase(app);
        }
        private static void UpdateDatabase(IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope();
            
            var serviceProvider = serviceScope.ServiceProvider;

            var migrationService = serviceProvider.GetService<MigrationService>();

            migrationService.Migrate().Wait();
        }
    }
}
