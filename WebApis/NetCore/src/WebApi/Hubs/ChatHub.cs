using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using WebApi.Attributes;
using WebApi.Controllers;
using WebApi.DTO.Mapping;

namespace WebApi.Hubs
{
    [IsAuthorization]
    public class ChatHub : Hub
    {
        /// <summary>
        /// key - connectionId
        /// value - chatId
        /// </summary>
        private static readonly ConcurrentDictionary<string, int> ChatClients=new ConcurrentDictionary<string, int>();

        private readonly ILogger _logger;

        private const string UpdateUsersCountMethod = "setCountUsers";
        private const string AddMessageMethod = "addMessage";
        private const string DeleteMessageMethod = "deleteMessage";
        private const string DeleteChatMethod = "deleteChat";

        public ChatHub(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<ChatController>();
        }
        
        public override async Task OnConnectedAsync()
        {
            try
            {
                await base.OnConnectedAsync();
            
                var connectionId = Context.ConnectionId;
            
                var query = Context.GetHttpContext().Request.Query["chatId"];
                var chatId = Convert.ToInt32(query);
            
                if (!ChatClients.TryAdd(connectionId, chatId))
                {
                    ChatClients.Remove(connectionId, out _);
                    ChatClients.TryAdd(connectionId, chatId);
                }

                var users = ChatClients.Where(r => r.Value == chatId).Select(r=>r.Key).ToList();

                //Обновляем количество пользователей в Чате
                if (users.Count != 0)
                {
                    await Clients.Clients(users)
                        .SendAsync(UpdateUsersCountMethod, users.Count);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e,"error in OnConnect");
                throw;
            }
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            try
            {
                var connectionId = Context.ConnectionId;
            
                await base.OnDisconnectedAsync(exception);

                //Оповещаем всех о выходе из Чата
                if (ChatClients.TryGetValue(connectionId, out int chatId))
                {
                    ChatClients.Remove(connectionId, out int _);
                    var users = ChatClients.Where(r => r.Value == chatId)
                        .Select(r=>r.Key)
                        .ToList();

                    if (users.Count != 0)
                    {
                        await Clients.Clients(users)
                            .SendAsync(UpdateUsersCountMethod, users.Count);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e,"error in OnDisconnect");
                throw;
            }
        }

        public async Task AddMessageAsync(ChatMessagesDto message)
        {
            try
            {
                var users = ChatClients.Where(r => r.Value == message.ChatId)
                    .Select(r=>r.Key)
                    .ToList();

                if (users.Count != 0)
                {
                    await Clients.Clients(users).SendAsync(AddMessageMethod, message);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "error in socket add Message");
                throw;
            }
        }

        public async Task DeleteMessage(int messageId, int chatId)
        {
            try
            {
                var users = ChatClients.Where(r => r.Value == chatId)
                    .Select(r=>r.Key)
                    .ToList();
                
                if (users.Count != 0)
                {
                    await Clients.Clients(users).SendAsync(DeleteMessageMethod, messageId);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "error in socket delete message");
                throw;
            }
        }
        public async Task DeleteChatAsync(int chatId)
        {
            try
            {
                var users = ChatClients.Where(r => r.Value == chatId)
                    .Select(r=>r.Key)
                    .ToList();
                
                if (users.Count != 0)
                {
                    await Clients.Clients(users).SendAsync(DeleteChatMethod);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "error in socket delete chat");
                throw;
            }
        }
    }
}