import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { WebApiHttpService } from '../services/web-api-http.service';
import { AuthorizationService } from '../services/authorization/authorization.service';
import { map } from 'rxjs/operators';
import { of, Observable } from 'rxjs';

@Injectable()
export class UserResolverService implements Resolve<any> {

  constructor(private readonly auth: AuthorizationService,
              private readonly webApi: WebApiHttpService) { }

  resolve(): Observable <boolean> {

    const { auth, webApi } = this;

    if (webApi.token && auth.user) { // if user is authorized

      return  of(true);
    } else if (!webApi.token) { // If user is not authorized

      auth.user = null;

      return of(false);
    } else { // if token is existing

      return auth.getUserByByToken(this.webApi.token).pipe(
        map(val => {
          if (!val) { // if response is not valid

            webApi.token = null;
            auth.user = null;

            return false;
          }

          auth.login.next(val.login);
          auth.user = val;

          return true;
        })
      );
    }
  }
}
