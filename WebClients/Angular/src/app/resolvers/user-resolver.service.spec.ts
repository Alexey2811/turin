import { TestBed } from '@angular/core/testing';

import { UserResolverService } from './user-resolver.service';
import { AppModule } from '../app.module';

describe('UserResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      AppModule
    ]
  }));

  it('should be created', () => {
    const service: UserResolverService = TestBed.get(UserResolverService);
    expect(service).toBeTruthy();
  });
});
