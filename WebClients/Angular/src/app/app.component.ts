import { Component } from '@angular/core';
import { TranslateServiceWebApi } from './services-ui/translate-service-web-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Angular';
  constructor(private readonly tr: TranslateServiceWebApi) {
    this.tr.setDefaultLang();
  }
}
