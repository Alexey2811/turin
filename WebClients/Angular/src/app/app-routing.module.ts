import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexAppComponent } from './components/index-app/index-app.component';
import { ErrorComponent } from './components/error/error.component';
import { UserResolverService } from './resolvers/user-resolver.service';
import { CanActiveService } from './can-active/can-active.service';


const routes: Routes = [
  {
    path: '',
    component: IndexAppComponent,
    resolve: { user: UserResolverService }
  },
  {
    path: 'books',
    loadChildren: () => import('src/app/modules/books/books.module').then(m => m.BooksModule),
    resolve: { user: UserResolverService },
    canActivate: [
      CanActiveService
    ]
  },
  {
    path: 'chats',
    loadChildren: () => import('src/app/modules/chat/chats.module').then(m => m.ChatsModule),
    resolve: { user: UserResolverService },
    canActivate: [
      CanActiveService
    ]
  },
  {
    path: '**',
    component: ErrorComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
