import { Injectable } from '@angular/core';
import { CanActivate, Router} from '@angular/router';
import { Observable } from 'rxjs';
import { UserResolverService } from '../resolvers/user-resolver.service';
import { map } from 'rxjs/operators';

@Injectable()
export class CanActiveService implements CanActivate {

  constructor(private readonly resolve: UserResolverService, 
              private readonly router: Router) { }

  canActivate = (): Observable<boolean> => 
    this.resolve.resolve().pipe(
      map(val => {
        if (!val){
          this.router.navigate(['/error']);
        }
        return val;
      })
    )
}
