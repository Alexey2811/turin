import { TestBed } from '@angular/core/testing';

import { CanActiveService } from './can-active.service';
import { AppModule } from '../app.module';

describe('CanActiveService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      AppModule
    ]
  }));

  it('should be created', () => {
    const service: CanActiveService = TestBed.get(CanActiveService);
    expect(service).toBeTruthy();
  });
});
