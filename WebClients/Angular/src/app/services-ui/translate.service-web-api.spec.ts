import { TestBed } from '@angular/core/testing';

import { TranslateServiceWebApi } from './translate-service-web-api.service';
import { AppModule } from '../app.module';

describe('TranslateService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports:[
      AppModule
    ]
  }));

  it('should be created', () => {
    const service: TranslateServiceWebApi = TestBed.get(TranslateServiceWebApi);
    expect(service).toBeTruthy();
  });

  it('should be setDefaultLang', () => {
    const service: TranslateServiceWebApi = TestBed.get(TranslateServiceWebApi);

    service.setDefaultLang();
    expect(service).toBeTruthy();
  });

  it('should be changeLang ru', () => {
    const service: TranslateServiceWebApi = TestBed.get(TranslateServiceWebApi);

    service.changeLang('ru');
    expect(service).toBeTruthy();
  });

  it('should be changeLang en', () => {
    const service: TranslateServiceWebApi = TestBed.get(TranslateServiceWebApi);

    service.changeLang('en');
    expect(service).toBeTruthy();
  });
});
