import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class TranslateServiceWebApi {

  private get lang() {
    return localStorage.getItem('lang');
  }
  private set lang(str: string) {
    localStorage.setItem('lang', str);
  }

  constructor(private readonly tr: TranslateService) { }

  changeLang = (str: 'ru' | 'en') => {
    this.tr.use(str);
    this.lang = str;
  }

  setDefaultLang() {
    const language = this.lang;
    if (language) {
      this.tr.use(language);
    } else {
      this.changeLang('en');
    }
  }
}
