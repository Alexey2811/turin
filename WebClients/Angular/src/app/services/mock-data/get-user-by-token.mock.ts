import { UserDto } from 'src/app/models/users/user-dto.interface';

export const GetUserByTokenMock: UserDto = {
    userId: 1,
    login: 'turinTest',
    roles: [
        {
            roleId: 1,
            name: 'Reader'
        }
    ]
};
