import { UserDto } from 'src/app/models/users/user-dto.interface';

export const GetUserByIdMock: UserDto = {
    userId: 3,
    login: 'turinTest3',
    roles: [
        {
            roleId: 1,
            name: 'Reader'
        }
    ]
};
