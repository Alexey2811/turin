import { UserDto } from 'src/app/models/users/user-dto.interface';

export const AddUserMock: UserDto = {
    userId: 2,
    login: 'turinTest2',
    roles: [
        {
            roleId: 1,
            name: 'Reader'
        }
    ]
};
