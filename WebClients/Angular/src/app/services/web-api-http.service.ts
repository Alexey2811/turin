import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { Observable, ObservableInput, throwError } from 'rxjs';
import { UriBuilder } from '../utils/uri-builder';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { WebAppNotificationsService } from '../modules/shared/services/web-app-notifications.service';

@Injectable()
export class WebApiHttpService {

  get token(): string {
    return localStorage.getItem('token');
  }

  set token(val: string) {
    if (val) {
      localStorage.setItem('token', val);
    } else {
      localStorage.removeItem('token');
    }
  }

  constructor(private readonly http: HttpClient,
    private readonly notification: WebAppNotificationsService) { }

  get(uri: string, params?: { [key: string]: any }) {

    const p = typeof params === 'undefined' ? {} : params;

    return this.http.get(`${UriBuilder.apiAddress}${uri}`,
      {
        params: p,
        headers: this.header
      })
      .pipe(
        map((r: Response) => r),
        catchError(this.errorHandle)
      );
  }

  post = (uri: string, req: any) =>
    this.http.post(`${UriBuilder.apiAddress}${uri}`, req, { headers: this.header })
      .pipe(
        map((r: Response) => r),
        catchError(this.errorHandle)
      )

  delete = (uri: string, request: any) =>
    this.http.delete(`${UriBuilder.apiAddress}${uri}`, {
      params: request,
      headers: this.header
    })
      .pipe(
        map((r: Response) => r),
        catchError(this.errorHandle)
      )

  put = (uri: string, request: any) =>
    this.http.put(`${UriBuilder.apiAddress}${uri}`, request, { headers: this.header })
      .pipe(
        map((r: Response) => r),
        catchError(this.errorHandle)
      )

  private get header(): HttpHeaders {
    let headers = new HttpHeaders(); // ... Set content type to JSON

    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('pragma', 'no-cache');
    headers = headers.append('cache-control', 'no-cache');

    if (this.token) {
      headers = headers.append('token', this.token);
    }

    return headers;
  }

  errorHandle = (err: any, caught: Observable<any>): ObservableInput<any> => {
    this.notification.showAlert(err.error.message);
    return throwError(err);
  }
}
