import { TestBed } from '@angular/core/testing';

import { WebApiHttpService } from './web-api-http.service';
import { AppModule } from '../app.module';

describe('WebApiHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      AppModule
    ]
  }));

  it('should be created', () => {
    const service: WebApiHttpService = TestBed.get(WebApiHttpService);
    expect(service).toBeTruthy();
  });
});
