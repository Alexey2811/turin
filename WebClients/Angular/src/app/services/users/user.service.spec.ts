import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import { WebApiHttpService } from '../web-api-http.service';
import { MockWebApiHttpService } from '../mock-web-api-http.service';

describe('UserService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
    providers: [
      {
        provide: WebApiHttpService,
        useClass: MockWebApiHttpService
      },
      UserService
    ]
  }));

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  it('should be getUserById', (done: DoneFn) => {
    const service: UserService = TestBed.get(UserService);

    const res = service.getUserById(1);
    res.subscribe(obj => {
      expect(obj.login).toEqual('turinTest3');
      done();
    });
  });

  it('should be addUser', (done: DoneFn) => {
    const service: UserService = TestBed.get(UserService);

    const res = service.addUser({
      login: 'temp',
      password: 'tempPassword'
    });
    res.subscribe(obj => {
      expect(obj.login).toEqual('turinTest2');
      done();
    });
  });

  it('should be updateUser', (done: DoneFn) => {
    const service: UserService = TestBed.get(UserService);

    const res = service.updateUser({
      login: 'temp',
      password: 'tempPassword',
      userId: 1
    });
    res.subscribe(obj => {
      expect(obj).toBeTruthy();
      done();
    });
  });

  it('should be isExistUser', (done: DoneFn) => {
    const service: UserService = TestBed.get(UserService);

    const res = service.isExistUser('turin');
    res.subscribe(obj => {
      expect(obj).toBeFalsy();
      done();
    });
  });
});
