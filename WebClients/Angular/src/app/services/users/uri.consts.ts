export const AddUserUri = 'users/addUser';
export const UpdateUserUri = 'users/updateUser';
export const GetUserByIdUri = 'users/getUserById';
export const IsExistLoginUri = 'users/isExistUser';
