import { Injectable } from '@angular/core';
import { WebApiHttpService } from '../web-api-http.service';
import { AddUserModel } from 'src/app/models/users/add-user-model.interface';
import { AddUserUri, UpdateUserUri, GetUserByIdUri, IsExistLoginUri } from './uri.consts';
import { UpdateUserModel } from 'src/app/models/users/update-user-model.interface';
import { Observable } from 'rxjs';
import { UserDto } from 'src/app/models/users/user-dto.interface';
import { TokenModel } from '../../models/token-model';

@Injectable()
export class UserService {

  constructor(private readonly http: WebApiHttpService) { }

  addUser = (model: AddUserModel): Observable<UserDto> =>
    this.http.put(AddUserUri, model)

  updateUser = (model: UpdateUserModel): Observable<TokenModel> =>
    this.http.post(UpdateUserUri, model)

  getUserById = (t: number): Observable<UserDto> =>
    this.http.get(GetUserByIdUri, { token: t })

  isExistUser = (l: string): Observable<boolean> =>
    this.http.get(IsExistLoginUri, { login: l });
}
