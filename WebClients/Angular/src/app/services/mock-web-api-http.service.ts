import { SignInUri, SignOutUri, GetUserByTokenUri } from './authorization/uri.consts';
import {of, Observable } from 'rxjs';
import {delay, tap} from 'rxjs/operators';
import { GetUserByTokenMock } from './mock-data/get-user-by-token.mock';
import { AddUserUri, UpdateUserUri, GetUserByIdUri, IsExistLoginUri } from './users/uri.consts';
import { AddUserMock } from './mock-data/add-user.mock';
import { GetUserByIdMock } from './mock-data/get-user-by-id-mock';
import { GetBooksUri, AddBookUri, UpdateBookUri, DeleteBookUri, GetBookByIdUri } from '../modules/books/services/uri.consts';
import { getBooksMock } from '../modules/books/services/mock-data/get-books.mock';
import { AddBookMock } from '../modules/books/services/mock-data/add-book.mock';
import { GetBookByIdMock } from '../modules/books/services/mock-data/get-book-by-id.mock';
import { GetMyChatsUri,
  GetAllChatsUri,
  DeleteChatUri,
  AddChatUri,
  UpdateChatUri,
  GetChatByIdUri } from '../modules/chat/services/uri.consts';
import { GetMyChatsMock } from '../modules/chat/services/mock-data/get-my-chats.mock';
import { GetAllChatsMock } from '../modules/chat/services/mock-data/get-all-chats.mock';
import { AddChatMock } from '../modules/chat/services/mock-data/add-chat.mock';
import { GetChatByIdMock } from '../modules/chat/services/mock-data/get-chat-by-id.mock';
import { GetChatMessagesUri, AddChatMessageUri, DeleteChatMessageUri } from '../modules/chat/services/messages/uri.consts';
import { GetMessagesMock } from '../modules/chat/services/messages/mock-data/get-messages.mock';
import { AddChatMessageMock } from '../modules/chat/services/messages/mock-data/add-chat-message.mock';

export class MockWebApiHttpService {

  get token(): string {
    return localStorage.getItem('token');
  }

  set token(val: string) {
    if (val) {
      localStorage.setItem('token', val);
    } else {
      localStorage.removeItem('token');
    }
  }

  constructor() {}

  get(uri: string, params?: { [key: string]: any }) {
    const  {getObservable} = this;

    switch (uri) {
        // authorization
        case SignOutUri: return getObservable(null, SignOutUri);

        // User
        case GetUserByTokenUri: return getObservable(GetUserByTokenMock, GetUserByTokenUri);
        case GetUserByIdUri: return getObservable(GetUserByIdMock, GetUserByIdUri);
        case IsExistLoginUri: return getObservable(false, IsExistLoginUri);

        // Books
        case GetBookByIdUri: return getObservable(GetBookByIdMock, GetBookByIdUri);

        // Chats
        case GetChatByIdUri: return getObservable(GetChatByIdMock, GetChatByIdUri);
    }
  }

  post(uri: string, request: any) {
    const  {getObservable} = this;

    switch (uri) {
        // authorization
        case SignInUri: return getObservable( {
          token: 'tokenFromStorage'
        }, SignInUri);

        // user
        case UpdateUserUri: return getObservable('newToken-qwerweqrwer', UpdateUserUri);

        // Books
        case GetBooksUri: return getObservable(getBooksMock, GetBooksUri);
        case UpdateBookUri: return getObservable(null, UpdateBookUri);

        // Chats
        case GetMyChatsUri: return getObservable(GetMyChatsMock, GetMyChatsUri);
        case GetAllChatsUri: return getObservable(GetAllChatsMock, GetAllChatsUri);
        case UpdateChatUri: return getObservable(null, UpdateChatUri);

        // Messages
        case GetChatMessagesUri: return getObservable(GetMessagesMock, GetChatMessagesUri);
    }
  }
  delete(uri: string, request: any) {
    const  {getObservable} = this;

    switch (uri) {
      // Books
      case DeleteBookUri: return getObservable(null, DeleteBookUri);

      // Chats
      case DeleteChatUri: return getObservable(null, DeleteChatUri);

      // Messages
      case DeleteChatMessageUri: return getObservable(null, DeleteChatMessageUri);
    }
  }

  put(uri: string, request: any) {
    const  {getObservable} = this;

    switch (uri) {
      // user
      case AddUserUri: return getObservable(AddUserMock, AddUserUri);

      // Books
      case AddBookUri: return getObservable(AddBookMock, AddBookUri);

      // Chats
      case AddChatUri: return getObservable(AddChatMock, AddChatUri);

      // Messages
      case AddChatMessageUri: return getObservable(AddChatMessageMock, AddChatMessageUri);
    }
  }

  private getObservable = (obj: any|null, uri: string): Observable<any> =>
    of(obj).pipe(
      delay(100),
      tap(val => {
        console.log(uri);
        console.log(val);
      })
    )
}
