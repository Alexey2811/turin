import { TestBed } from '@angular/core/testing';

import { AuthorizationService } from './authorization.service';
import { WebApiHttpService } from '../web-api-http.service';
import { MockWebApiHttpService } from '../mock-web-api-http.service';

describe('AuthorizationService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
    providers: [
      {
        provide: WebApiHttpService,
        useClass: MockWebApiHttpService
      },
      AuthorizationService
    ]
  }));

  it('should be created', () => {
    const service: AuthorizationService = TestBed.get(AuthorizationService);
    expect(service).toBeTruthy();
  });

  it('should be SignIn', (done: DoneFn) => {
    const service: AuthorizationService = TestBed.get(AuthorizationService);

    const auth = service.signIn({login: 'turin', password: '123123'});
    auth.subscribe(str => {
      expect(str).toBeTruthy();
      done();
    });
  });

  it('should be SignOut', (done: DoneFn) => {
    const service: AuthorizationService = TestBed.get(AuthorizationService);

    const auth = service.signOut('token out');
    auth.subscribe(str => {
      expect(str).toBeUndefined();
      done();
    });
  });

  it('should be getUserByToken', (done: DoneFn) => {
    const service: AuthorizationService = TestBed.get(AuthorizationService);

    const auth = service.getUserByByToken('token out');
    auth.subscribe(str => {
      expect(str.login).toEqual('turinTest');
      done();
    });
  });
});
