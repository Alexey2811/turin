export const SignInUri = 'authorization/authorize';
export const SignOutUri = 'authorization/signOut';
export const GetUserByTokenUri = 'authorization/getUserByToken';
