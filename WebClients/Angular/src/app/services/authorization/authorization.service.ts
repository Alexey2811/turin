import { Injectable } from '@angular/core';
import { AuthorizationModel } from 'src/app/models/authorization/authorization-model.interface';
import { Observable, BehaviorSubject } from 'rxjs';
import { WebApiHttpService } from '../web-api-http.service';
import { SignOutUri, SignInUri, GetUserByTokenUri } from './uri.consts';
import { map } from 'rxjs/operators';
import { UserDto } from 'src/app/models/users/user-dto.interface';
import {TokenModel} from '../../models/token-model';

@Injectable()
export class AuthorizationService {

  private _user: UserDto;

  set user(val: UserDto) {

    this._user = val;
    if (val) {
      this.login.next(val.login);
    } else {
      this.login.next('');
    }
  }
  get user(): UserDto {
    return this._user;
  }

  login: BehaviorSubject<string> = new BehaviorSubject<string>('');

  constructor(private readonly http: WebApiHttpService) { }

  signIn(model: AuthorizationModel): Observable<TokenModel> {
    return this.http.post(SignInUri, model).pipe(
      map(token => {
        this.http.token = token.token;
        return token;
      })
    );
  }

  signOut(t: string): Observable<void> {
    return this.http.get(SignOutUri, {token: t}).pipe(
      map(() => {
        this.http.token = null;
        this.user = null;
        return;
      })
    );
  }

  getUserByByToken = (t: string): Observable<UserDto> =>
    this.http.get(GetUserByTokenUri, {tokenId: encodeURIComponent(t)})
}
