import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexBooksComponent } from './components/index-books/index-books.component';


const routes: Routes = [
  {
    path: '',
    component: IndexBooksComponent
  },
  {
    path: ':page',
    component: IndexBooksComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BooksRoutingModule { }
