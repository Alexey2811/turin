import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AddBookModel } from '../../models/add-book-model.interface';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.scss']
})
export class AddBookComponent implements OnInit {

  form: FormGroup;
  fn = (b: AddBookModel) => { };

  constructor(private readonly bsModalRef: BsModalRef) {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      author: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      description: new FormControl('', [Validators.required, Validators.maxLength(300)])
    });
  }

  ngOnInit() {
  }

  save() {
    if (this.form.valid) {
      const newBook: AddBookModel = {
        author: this.form.get('author').value,
        name: this.form.get('name').value,
        description: this.form.get('description').value,
      };
      this.fn(newBook);
      this.bsModalRef.hide();
    }
  }

  close() {
    this.bsModalRef.hide();
  }
}
