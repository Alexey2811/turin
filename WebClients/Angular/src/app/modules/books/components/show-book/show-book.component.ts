import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { BookDto } from '../../models/book-dto.interface';

@Component({
  selector: 'app-show-book',
  templateUrl: './show-book.component.html',
  styleUrls: ['./show-book.component.scss']
})
export class ShowBookComponent implements OnInit {

  book: BookDto = {
    name: '',
    description: '',
    author: '',
    creationDate: new Date(),
    bookId: 0
  };

  constructor(private readonly bsModalRef: BsModalRef) {
  }

  ngOnInit() {
  }

  close() {
    this.bsModalRef.hide();
  }
}
