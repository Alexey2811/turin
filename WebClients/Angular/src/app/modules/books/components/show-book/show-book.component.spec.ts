import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowBookComponent } from './show-book.component';
import { BooksModule } from '../../books.module';
import { AppModule } from 'src/app/app.module';

describe('ShowBookComponent', () => {
  let component: ShowBookComponent;
  let fixture: ComponentFixture<ShowBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BooksModule, AppModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
