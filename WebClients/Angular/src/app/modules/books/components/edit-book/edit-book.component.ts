import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BookDto } from '../../models/book-dto.interface';
import { UpdateBookModel } from '../../models/update-book-model.interface';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.scss']
})
export class EditBookComponent implements OnInit {

  constructor(private readonly bsModalRef: BsModalRef) {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      author: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      description: new FormControl('', [Validators.required, Validators.maxLength(300)])
    });
  }

  form: FormGroup;

  book: BookDto = {
    name: '',
    bookId: 0,
    author: '',
    description: '',
    creationDate: new Date()
  };

  fn = (model: UpdateBookModel) => {
  }

  ngOnInit() {
    this.form.get('name').setValue(this.book.name);
    this.form.get('author').setValue(this.book.author);
    this.form.get('description').setValue(this.book.description);
  }

  save() {
    if (this.form.valid) {
      const updateBook: UpdateBookModel = {
        author: this.form.get('author').value,
        name: this.form.get('name').value,
        description: this.form.get('description').value,
        bookId: this.book.bookId
      };
      this.fn(updateBook);
      this.bsModalRef.hide();
    }
  }

  close() {
    this.bsModalRef.hide();
  }
}
