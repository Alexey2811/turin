import {Component, OnInit} from '@angular/core';
import {BookService} from '../../services/book.service';
import {BehaviorSubject, of} from 'rxjs';
import {BookDto} from '../../models/book-dto.interface';
import {BsModalService, PageChangedEvent} from 'ngx-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {WebAppNotificationsService} from '../../../shared/services/web-app-notifications.service';
import {switchMap} from 'rxjs/operators';
import {ShowBookComponent} from '../show-book/show-book.component';
import {EditBookComponent} from '../edit-book/edit-book.component';
import {AddBookComponent} from '../add-book/add-book.component';
import {UpdateBookModel} from '../../models/update-book-model.interface';
import {AddBookModel} from '../../models/add-book-model.interface';
import {RequestPaginationModel} from '../../../../models/request-pagination-model.interface';

@Component({
  selector: 'app-index-books',
  templateUrl: './index-books.component.html',
  styleUrls: ['./index-books.component.scss']
})
export class IndexBooksComponent implements OnInit {

  count: number;
  page: number;
  size = 10;

  books = new Array<BookDto>();

  constructor(private readonly bookService: BookService,
              private readonly actRoute: ActivatedRoute,
              private readonly router: Router,
              private readonly modalService: BsModalService,
              private readonly notification: WebAppNotificationsService) {
    this.actRoute.paramMap.subscribe(params => {
      if (params.has('page')) {
        this.getBooks(+params.get('page'));
      } else {
        this.getBooks(1);
      }
    });
  }

  private getBooks(page: number) {
    const request: RequestPaginationModel = {
      page: page - 1,
      size: this.size
    };
    this.bookService.getBooks(request).subscribe(data => {
      this.count = data.count;
      this.books = data.items;
      setTimeout(() => {
        this.page = page;
      });
    });
  }

  ngOnInit() {

  }

  pageChanged(event: PageChangedEvent) {
    const page = event.page;
    this.router.navigate([`/books/${page}`]);
  }

  deleteBook(bookId: number) {
    this.notification.showConfirm('book_will_deleted', () => {
      this.bookService.deleteBook(bookId).subscribe(_ => {
        const newArr = [...this.books];

        const index = newArr.findIndex(r => r.bookId === bookId);

        if (index >= 0) {
          newArr.splice(index, 1);
        }
        this.books = newArr;
        this.count--;
      });
    });
  }

  showBook(bookId: number) {

    this.bookService.getBookById(bookId).subscribe(bookDto => {
      this.modalService.show(ShowBookComponent, {
        ignoreBackdropClick: true,
        initialState: {
          book: bookDto
        }
      });
    });
  }

  editHandler = (updateModel: UpdateBookModel) => {
    this.bookService.updateBook(updateModel).subscribe(() => {
      const arr = [...this.books];

      const book = arr.find(r => r.bookId === updateModel.bookId);
      book.name = updateModel.name;
      book.description = updateModel.description;
      book.author = updateModel.author;

      this.books = arr;
    });
  }

  editBook(bookId: number) {
    this.bookService.getBookById(bookId).subscribe(data => {
      this.modalService.show(EditBookComponent, {
        ignoreBackdropClick: true,
        initialState: {
          book: data,
          fn: this.editHandler,
        }
      });
    });
  }

  addBookHandle = (bookModel: AddBookModel) => {
    this.bookService.addBook(bookModel).subscribe(data => {
      const newArr = [...this.books, data];
      this.books = newArr;
    });
  }

  addBook() {
    this.modalService.show(AddBookComponent, {
      ignoreBackdropClick: true,
      initialState: {
        fn: this.addBookHandle
      }
    });
  }
}
