import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexBooksComponent } from './index-books.component';
import { BooksModule } from '../../books.module';
import { AppModule } from 'src/app/app.module';

describe('IndexBooksComponent', () => {
  let component: IndexBooksComponent;
  let fixture: ComponentFixture<IndexBooksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        BooksModule,
        AppModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexBooksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
