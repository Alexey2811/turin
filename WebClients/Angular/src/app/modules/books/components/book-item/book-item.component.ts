import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-book-item',
  templateUrl: './book-item.component.html',
  styleUrls: ['./book-item.component.scss']
})
export class BookItemComponent implements OnInit {

  @Input() title = '';
  @Input() author = '';
  @Input() description = '';
  @Input() creationDate = new Date();
  @Input() bookId: number;

  @Output() deleteBook: EventEmitter<number> = new EventEmitter<number>();
  @Output() editBook: EventEmitter<number> = new EventEmitter<number>();
  @Output() showBook: EventEmitter<number> = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }
}
