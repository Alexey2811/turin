export const GetBookByIdUri = 'books/getBookById';
export const GetBooksUri = 'books/getBooks';
export const DeleteBookUri = 'books/deleteBook';
export const UpdateBookUri = 'books/updateBook';
export const AddBookUri = 'books/addBook';
