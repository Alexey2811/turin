import { TestBed } from '@angular/core/testing';

import { BookService } from './book.service';
import { WebApiHttpService } from 'src/app/services/web-api-http.service';
import { MockWebApiHttpService } from 'src/app/services/mock-web-api-http.service';

describe('BookService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
    providers: [
      {
        provide: WebApiHttpService,
        useClass: MockWebApiHttpService
      },
      BookService
    ]
  }));

  it('should be created', () => {
    const service: BookService = TestBed.get(BookService);
    expect(service).toBeTruthy();
  });

  it('should be addBook', (done: DoneFn) => {
    const service: BookService = TestBed.get(BookService);

    const check = service.addBook({
      name: 'tempBook',
      description: 'tempDescription',
      author: 'I'
    });
    check.subscribe(str => {
      expect(str.name).toEqual('tempBook');
      done();
    });
  });

  it('should be deleteBook', (done: DoneFn) => {
    const service: BookService = TestBed.get(BookService);

    const check = service.deleteBook(1);
    check.subscribe(str => {
      expect(str).toBeNull();
      done();
    });
  });

  it('should be getBookById', (done: DoneFn) => {
    const service: BookService = TestBed.get(BookService);

    const check = service.getBookById(1);
    check.subscribe(str => {
      expect(str.name).toEqual('tempBookupdate');
      done();
    });
  });

  it('should be getBooks', (done: DoneFn) => {
    const service: BookService = TestBed.get(BookService);

    const check = service.getBooks({
      page: 0,
      size: 10
    });
    check.subscribe(str => {
      expect(str.count).toBeGreaterThan(0);
      done();
    });
  });

  it('should be updateBook', (done: DoneFn) => {
    const service: BookService = TestBed.get(BookService);

    const check = service.updateBook({
      author: 'newAuthor',
      name: 'newName',
      description: 'new Desc',
      bookId: 1
    });
    check.subscribe(str => {
      expect(str).toBeNull();
      done();
    });
  });
});
