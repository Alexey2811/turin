import { ResponsePaginationModel } from 'src/app/models/response-pagination-model.interface';
import { BookDto } from '../../models/book-dto.interface';

export const getBooksMock: ResponsePaginationModel<BookDto> = {
    count: 2,
    items: [
        {
            bookId: 1,
            name: 'tempBook',
            description: 'tempDescription',
            creationDate: new Date(),
            author: 'turin'
        },
        {
            bookId: 2,
            name: 'tempBook2',
            description: 'tempDescription2',
            creationDate: new Date(),
            author: 'turin2'
        }
    ]
};
