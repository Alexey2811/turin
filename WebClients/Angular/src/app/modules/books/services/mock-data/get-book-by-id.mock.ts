import { BookDto } from '../../models/book-dto.interface';

export const GetBookByIdMock: BookDto = {
    bookId: 1,
    name: 'tempBookupdate',
    description: 'tempDescriptionupdate',
    creationDate: new Date(),
    author: 'turinUpdate'
};
