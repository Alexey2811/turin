import { BookDto } from '../../models/book-dto.interface';

export const AddBookMock: BookDto = {
    bookId: 1,
    name: 'tempBook',
    description: 'tempDescription',
    creationDate: new Date(),
    author: 'turin'
};
