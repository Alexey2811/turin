import { Injectable } from '@angular/core';
import { WebApiHttpService } from 'src/app/services/web-api-http.service';
import { RequestPaginationModel } from 'src/app/models/request-Pagination-model.interface';
import { Observable } from 'rxjs';
import { ResponsePaginationModel } from 'src/app/models/response-Pagination-model.interface';
import { BookDto } from '../models/book-dto.interface';
import { GetBooksUri, AddBookUri, UpdateBookUri, DeleteBookUri, GetBookByIdUri } from './uri.consts';
import { AddBookModel } from '../models/add-book-model.interface';
import { UpdateBookModel } from '../models/update-book-model.interface';

@Injectable()
export class BookService {

  constructor(private readonly http: WebApiHttpService) { }

  getBooks = (request: RequestPaginationModel): Observable<ResponsePaginationModel<BookDto>> =>
    this.http.post(GetBooksUri, request)

  addBook = (request: AddBookModel): Observable<BookDto> =>
    this.http.put(AddBookUri, request)

  updateBook = (request: UpdateBookModel): Observable<void> =>
    this.http.post(UpdateBookUri, request)

  deleteBook = (id: number): Observable<void> =>
    this.http.delete(DeleteBookUri, {bookId: id})

  getBookById = (id: number): Observable<BookDto> =>
    this.http.get(GetBookByIdUri, {bookId: id})
}
