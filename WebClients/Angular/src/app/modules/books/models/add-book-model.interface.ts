export interface AddBookModel {
    name: string;
    description: string;
    author: string;
}
