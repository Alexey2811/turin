export interface UpdateBookModel {
    bookId: number;
    author: string;
    description: string;
    name: string;
}
