export interface BookDto {
    bookId: number;
    name: string;
    description: string;
    author: string;
    creationDate: Date;
}
