import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BooksRoutingModule } from './books-routing.module';
import { BookService } from './services/book.service';
import { IndexBooksComponent } from './components/index-books/index-books.component';
import { BookItemComponent } from './components/book-item/book-item.component';
import { SharedModule } from '../shared/shared.module';
import { ShowBookComponent } from './components/show-book/show-book.component';
import { EditBookComponent } from './components/edit-book/edit-book.component';
import { AddBookComponent } from './components/add-book/add-book.component';
import { ModalModule } from 'ngx-bootstrap';


@NgModule({
  declarations: [IndexBooksComponent,
    BookItemComponent,
    ShowBookComponent,
    EditBookComponent,
    AddBookComponent],
  entryComponents: [
    ShowBookComponent,
    EditBookComponent,
    AddBookComponent
  ],
  imports: [
    CommonModule,
    BooksRoutingModule,
    SharedModule,
    ModalModule.forRoot()
  ],
  providers: [BookService]
})
export class BooksModule {
}
