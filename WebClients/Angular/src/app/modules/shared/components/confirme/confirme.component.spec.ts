import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmeComponent } from './confirme.component';
import { AppModule } from 'src/app/app.module';

describe('ConfirmeComponent', () => {
  let component: ConfirmeComponent;
  let fixture: ComponentFixture<ConfirmeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
