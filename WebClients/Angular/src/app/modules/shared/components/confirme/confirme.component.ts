import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-confirme',
  templateUrl: './confirme.component.html',
  styleUrls: ['./confirme.component.scss']
})
export class ConfirmeComponent implements OnInit {

  constructor(public bsModalRef: BsModalRef) { }
  text: string;

  yes = () => { };
  no = () => { };

  ngOnInit() {
  }
  close() {
    if (this.no) {
      this.no();
    }
    this.bsModalRef.hide();
  }
  apply() {
    if (this.yes) {
      this.yes();
    }
    this.bsModalRef.hide();
  }
}
