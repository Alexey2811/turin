import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

  constructor(public bsModalRef: BsModalRef) { }
  text: string;

  ngOnInit() {
  }
  close() {
    this.bsModalRef.hide();
  }
}
