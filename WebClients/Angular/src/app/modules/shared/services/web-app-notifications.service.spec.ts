import { TestBed } from '@angular/core/testing';

import { WebAppNotificationsService } from './web-app-notifications.service';
import { AppModule } from 'src/app/app.module';

describe('WebAppNotificationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [AppModule]
  }));

  it('should be created', () => {
    const service: WebAppNotificationsService = TestBed.get(WebAppNotificationsService);
    expect(service).toBeTruthy();
  });
});
