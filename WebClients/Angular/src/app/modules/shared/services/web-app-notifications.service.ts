import { Injectable } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { ConfirmeComponent } from '../components/confirme/confirme.component';
import { AlertComponent } from '../components/alert/alert.component';

@Injectable()
export class WebAppNotificationsService {

  constructor(private readonly modalService: BsModalService) { }

  showConfirm(t: string, y?: () => void, n?: () => void) {
    const init = {
      yes: y,
      no: n,
      text: t
    };
    this.modalService.show(ConfirmeComponent, {
      ignoreBackdropClick: true,
      initialState: init
    });
  }
  showAlert = (t: string) => {
    const init = {
      text: t
    };
    this.modalService.show(AlertComponent, {
      ignoreBackdropClick: true,
      initialState: init
    });
  }
}
