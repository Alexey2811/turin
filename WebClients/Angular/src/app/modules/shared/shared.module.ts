import { NgModule } from '@angular/core';
import { BsDropdownModule, AlertModule, PaginationModule } from 'ngx-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { MomentModule } from 'ngx-moment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmeComponent } from './components/confirme/confirme.component';
import { AlertComponent } from './components/alert/alert.component';
import { WebAppNotificationsService } from './services/web-app-notifications.service';


@NgModule({
  imports: [
    TranslateModule,
  ],
  exports: [
    BsDropdownModule,
    AlertModule,
    PaginationModule,
    TranslateModule,
    MomentModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [ConfirmeComponent, AlertComponent],
  entryComponents: [ConfirmeComponent, AlertComponent],
  providers: [
    WebAppNotificationsService
  ]
})
export class SharedModule {
}
