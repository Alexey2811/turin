import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChatsRoutingModule } from './chats-routing.module';
import { ChatService } from './services/chat.service';
import { ChatMessageService } from './services/messages/chat-message.service';
import { WebSocketService } from './services/signalR/web-socket.service';
import { environment } from 'src/environments/environment';
import { MockWebSocketService } from './services/signalR/mock-web-socket.service';
import { IndexChatComponent } from './components/index-chat/index-chat.component';
import {ModalModule} from 'ngx-bootstrap';
import {ChatItemComponent} from './components/item-chat/chat-item.component';
import {SharedModule} from '../shared/shared.module';
import { ShowChatComponent } from './components/show-chat/show-chat.component';
import { EditChatComponent } from './components/edit-chat/edit-chat.component';
import { AddChatComponent } from './components/add-chat/add-chat.component';


@NgModule({
  declarations: [IndexChatComponent,
    ChatItemComponent,
    ShowChatComponent,
    EditChatComponent,
    AddChatComponent],
  entryComponents: [
    ShowChatComponent,
    EditChatComponent,
    AddChatComponent
  ],
  imports: [
    CommonModule,
    ChatsRoutingModule,
    SharedModule,
    ModalModule.forRoot(),
  ],
  providers: [
    ChatService,
    ChatMessageService, {
      provide: WebSocketService,
      useClass: environment.mock ?
        MockWebSocketService : WebSocketService
  }]
})
export class ChatsModule { }
