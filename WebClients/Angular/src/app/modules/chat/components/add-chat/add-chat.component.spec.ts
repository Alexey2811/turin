import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChatComponent } from './add-chat.component';
import { AppModule } from 'src/app/app.module';
import { ChatsModule } from '../../chats.module';

describe('AddChatComponent', () => {
  let component: AddChatComponent;
  let fixture: ComponentFixture<AddChatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, ChatsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
