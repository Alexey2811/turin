import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AddChatModel } from '../../models/add-chat-model.interface';

@Component({
  selector: 'app-add-chat',
  templateUrl: './add-chat.component.html',
  styleUrls: ['./add-chat.component.scss']
})
export class AddChatComponent implements OnInit {

  constructor(private readonly bsModalRef: BsModalRef) {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.maxLength(100)])
    });
  }

  form: FormGroup;

  creatorId = 0;

  fn = (chatModel: AddChatModel) => { };

  ngOnInit() {
  }

  close() {
    this.bsModalRef.hide();
  }
  save() {
    if (this.form.valid) {
      const newChat: AddChatModel = {
        name: this.form.get('name').value,
        creatorId: this.creatorId
      }
      this.fn(newChat);
      this.bsModalRef.hide();
    }
  }
}
