import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-chat-item',
  templateUrl: './chat-item.component.html',
  styleUrls: ['./chat-item.component.scss']
})
export class ChatItemComponent implements OnInit {

  @Input() chatId: number;
  @Input() name: string;
  @Input() creatorId: number;
  @Input() creatorName: string;
  @Input() currentUserId: number;

  @Output() editChat: EventEmitter<number> = new EventEmitter<number>();
  @Output() showChat: EventEmitter<number> = new EventEmitter<number>();
  @Output() deleteChat: EventEmitter<number> = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit() {
  }

  get isMainChat(): boolean {
    return this.currentUserId === this.creatorId;
  }

}
