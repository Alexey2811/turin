import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UpdateChatModel } from '../../models/update-chat-model.interface';
import { ChatDto } from '../../models/chat-dto.interface';

@Component({
  selector: 'app-edit-chat',
  templateUrl: './edit-chat.component.html',
  styleUrls: ['./edit-chat.component.scss']
})
export class EditChatComponent implements OnInit {
  form: FormGroup;

  constructor(private readonly bsModalRef: BsModalRef) {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.maxLength(100)])
    });
  }

  chat: ChatDto = {
    chatId: 0,
    name: '',
    creatorName: '',
    creatorId: 0
  };

  fn = (updateModel: UpdateChatModel) => {
  }

  ngOnInit() {
    this.form.get('name').setValue(this.chat.name);
  }

  close() {
    this.bsModalRef.hide();
  }

  save() {
    const updateModel: UpdateChatModel = {
      chatId: this.chat.chatId,
      chatName: this.form.get('name').value
    };
    this.fn(updateModel);
    this.bsModalRef.hide();
  }
}
