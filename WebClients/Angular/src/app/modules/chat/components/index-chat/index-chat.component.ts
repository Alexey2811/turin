import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService, PageChangedEvent } from 'ngx-bootstrap';
import { WebAppNotificationsService } from '../../../shared/services/web-app-notifications.service';
import { ChatDto } from '../../models/chat-dto.interface';
import { ChatService } from '../../services/chat.service';
import { RequestPaginationModel } from '../../../../models/request-pagination-model.interface';
import { AuthorizationService } from '../../../../services/authorization/authorization.service';
import { RequestPaginationWithUserIdModel } from '../../models/request-pagination-with-userid-model.interface';
import { UpdateChatModel } from '../../models/update-chat-model.interface';
import { AddChatModel } from '../../models/add-chat-model.interface';
import { AddChatComponent } from '../add-chat/add-chat.component';
import { ShowChatComponent } from '../show-chat/show-chat.component';
import { EditChatComponent } from '../edit-chat/edit-chat.component';
import { ResponsePaginationModel } from '../../../../models/response-pagination-model.interface';

@Component({
  selector: 'app-index-chat',
  templateUrl: './index-chat.component.html',
  styleUrls: ['./index-chat.component.scss']
})
export class IndexChatComponent implements OnInit {

  count: number;
  page = 1;
  size = 10;

  chats = new Array<ChatDto>();

  currentUser: number;
  isMyChats = false;

  constructor(private readonly chatService: ChatService,
    private readonly actRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly user: AuthorizationService,
    private readonly modalService: BsModalService,
    private readonly notification: WebAppNotificationsService) {
    this.actRoute.paramMap.subscribe(params => {
      if (params.has('page')) {
        this.isMyChats ?
          this.getChatsMy(+params.get('page')) :
          this.getChatsAll(+params.get('page'));
      } else {
        this.isMyChats ?
          this.getChatsMy(1) :
          this.getChatsAll(1);
      }
      if (this.user.user) {
        const { userId } = this.user.user;
        this.currentUser = userId ? userId : null;
      }
    });
  }

  getCheckedChats() {
    if (this.page === 1) {
      this.isMyChats ?
        this.getChatsMy(1) :
        this.getChatsAll(1);
    } else {
      this.router.navigate([`/chats/1`]);
      this.page = 1;
    }
  }

  private updateChatsInner = (chat: ResponsePaginationModel<ChatDto>, page: number) => {
    this.count = chat.count;
    this.chats = chat.items;
    setTimeout(() => {
      this.page = page;
    });
  }

  private getChatsAll(page: number) {
    const request: RequestPaginationModel = {
      page: page - 1,
      size: this.size
    };
    this.chatService.getAllChats(request).subscribe(data => this.updateChatsInner(data, page));
  }

  private getChatsMy(page: number) {
    const request: RequestPaginationWithUserIdModel = {
      page: page - 1,
      size: this.size,
      userId: this.currentUser
    };
    this.chatService.getMyChats(request).subscribe(data => this.updateChatsInner(data, page));
  }

  deleteChat(chatId: number) {
    this.notification.showConfirm('chat_will_deleted', () => {
      this.chatService.deleteChat(chatId).subscribe(arr => {
        const newArr = [...this.chats];

        const index = newArr.findIndex(r => r.chatId === chatId);

        if (index >= 0) {
          newArr.splice(index, 1);
        }
        this.chats = newArr;
        this.count--;
      });
    });
  }

  ngOnInit() {
  }

  pageChanged(event: PageChangedEvent) {
    const page = event.page;
    this.router.navigate([`/chats/${page}`]);
  }

  addChatHandler = (catModel: AddChatModel) => {
    this.chatService.addChat(catModel).subscribe(data => {
      const newArr = [...this.chats, data];
      this.chats = newArr;

      this.count++;
    });
  }

  addChat() {
    this.modalService.show(AddChatComponent, {
      ignoreBackdropClick: true,
      initialState: {
        fn: this.addChatHandler,
        creatorId: this.user.user.userId
      }
    });
  }

  editChatHandler = (c: UpdateChatModel) => {
    this.chatService.updateChat(c).subscribe(d => {
      const arr = [...this.chats];

      const chat = arr.find(r => r.chatId === c.chatId);
      chat.name = c.chatName;

      this.chats = arr;
    });
  }

  editChat(i: number) {
    this.chatService.getChatById(i).subscribe(d => {
      this.modalService.show(EditChatComponent, {
        ignoreBackdropClick: true,
        initialState: {
          chat: d,
          fn: this.editChatHandler
        }
      });
    });
  }

  showChat(i: number) {
    this.modalService.show(ShowChatComponent, {
      ignoreBackdropClick: true,
      initialState: {
        chatId: i
      }
    });
  }
}
