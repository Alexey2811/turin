import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexChatComponent } from './index-chat.component';
import { ChatsModule } from '../../chats.module';
import { AppModule } from 'src/app/app.module';

describe('IndexChatComponent', () => {
  let component: IndexChatComponent;
  let fixture: ComponentFixture<IndexChatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ChatsModule,
        AppModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
