import { AfterViewInit, Component, ElementRef, HostListener, OnDestroy, ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { ChatMessageService } from '../../services/messages/chat-message.service';
import { ChatMessagesDto } from '../../models/chat-message-dto.interface';
import { AuthorizationService } from '../../../../services/authorization/authorization.service';
import { RequestPaginationWithChatIdModel } from '../../models/request-pagination-with-chatid-model.interface';
import { WebSocketService } from '../../services/signalR/web-socket.service';
import { AddChatMessageModel } from '../../models/add-chat-message-model.interface';

@Component({
  selector: 'app-show-chat',
  templateUrl: './show-chat.component.html',
  styleUrls: ['./show-chat.component.scss']
})
export class ShowChatComponent implements OnDestroy, AfterViewInit {

  constructor(private readonly bsModalRef: BsModalRef,
    private readonly socket: WebSocketService,
    private readonly chatMessagesService: ChatMessageService,
    private readonly userService: AuthorizationService) {

    if (this.userService.user) {
      this.userId = this.userService.user.userId;
    }
  }

  @ViewChild('scrollContainer', { static: true }) private scroll: ElementRef;

  messages = new Array<ChatMessagesDto>();

  chatId = 0;

  page = 0;
  size = 100;
  count = 0;

  userId = 0;

  countUsers = 0;

  showChatDeleted = false;
  message = '';

  @HostListener('scroll', ['$event'])
  onScroll(event: any) {
    if (!event.target.scrollTop &&
      this.count &&
      this.messages &&
      this.messages.length < this.count) {

      this.page++;
      const request: RequestPaginationWithChatIdModel = {
        page: this.page,
        size: this.size,
        chatId: this.chatId
      };
      this.chatMessagesService.getMessages(request).subscribe(data => {
        this.count = data.count;

        const newArr = [...data.items, ...this.messages];

        this.messages = newArr;

        setTimeout(() => {
          this.scroll.nativeElement.scrollTop = 100;
        });
      });
    }
  }

  isMainMessage = (creatorId: number) => this.userId === creatorId;

  setCountUsersMethod = (count: number) => {
    this.countUsers = count;
  }
  addMessageMethod = (message: ChatMessagesDto) => {
    const newArr = [...this.messages, message];

    this.messages = newArr;
  }

  deleteMessageMethod = (id: number) => {
    const newArr = [...this.messages];

    const index = newArr.findIndex(r => r.chatMessageId === id);

    if (index >= 0) {
      newArr.splice(index, 1);
    }
    this.messages = newArr;
    this.count--;
  }
  deleteChatMethod = () => {
    this.showChatDeleted = true;
  }

  getMessages() {
    const request: RequestPaginationWithChatIdModel = {
      page: this.page,
      size: this.size,
      chatId: this.chatId
    };
    this.chatMessagesService.getMessages(request).subscribe(data => {
      this.messages = data.items;
      this.count = data.count;

      setTimeout(() => {
        this.scroll.nativeElement.scrollTop = this.scroll.nativeElement.scrollHeight;
      });
    });
  }

  close() {
    this.bsModalRef.hide();
  }

  addMessageServer() {
    if (this.message) {
      const addModel: AddChatMessageModel = {
        message: this.message,
        chatId: this.chatId,
        creatorId: this.userId,
      };
      this.chatMessagesService.addChatMessages(addModel).subscribe(() => {
        this.message = '';
        this.scroll.nativeElement.scrollTop = this.scroll.nativeElement.scrollHeight;
      });
    }
  }

  deleteMessageServer(id: number) {
    this.chatMessagesService.deleteMessage(id).subscribe(() => {
    });
  }

  ngOnDestroy() {
    this.socket.disconnect();
  }

  ngAfterViewInit(): void {
    this.socket.connect(this.chatId,
      this.setCountUsersMethod,
      this.addMessageMethod,
      this.deleteMessageMethod,
      this.deleteChatMethod);
      
    this.getMessages();
  }
}
