import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowChatComponent } from './show-chat.component';
import { AppModule } from 'src/app/app.module';
import { ChatsModule } from '../../chats.module';

describe('ShowChatComponent', () => {
  let component: ShowChatComponent;
  let fixture: ComponentFixture<ShowChatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, ChatsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
