import { ResponsePaginationModel } from 'src/app/models/response-pagination-model.interface';
import { ChatDto } from '../../models/chat-dto.interface';

export const GetMyChatsMock: ResponsePaginationModel<ChatDto> = {
    count: 2,
    items: [
        {
            chatId: 1,
            name: 'name',
            creatorId: 1,
            creatorName: 'turin'
        },
        {
            chatId: 2,
            name: 'name2',
            creatorId: 1,
            creatorName: 'turin2'
        }
    ]
};
