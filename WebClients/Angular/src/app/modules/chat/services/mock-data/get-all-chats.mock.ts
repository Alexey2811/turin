import { ResponsePaginationModel } from 'src/app/models/response-pagination-model.interface';
import { ChatDto } from '../../models/chat-dto.interface';

export const GetAllChatsMock: ResponsePaginationModel<ChatDto> = {
    count: 2,
    items: [
        {
            chatId: 1,
            name: 'name',
            creatorId: 1,
            creatorName: 'turin'
        },
        {
            chatId: 2,
            name: 'name2',
            creatorId: 2,
            creatorName: 'turin2'
        },
      {
        chatId: 3,
        name: 'name2',
        creatorId: 2,
        creatorName: 'turin2'
      },
      {
        chatId: 4,
        name: 'name2',
        creatorId: 2,
        creatorName: 'turin2'
      },
      {
        chatId: 5,
        name: 'name2',
        creatorId: 2,
        creatorName: 'turin2'
      }
    ]
};
