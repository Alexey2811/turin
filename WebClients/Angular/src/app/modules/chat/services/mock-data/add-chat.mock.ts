import { ChatDto } from '../../models/chat-dto.interface';

export const AddChatMock: ChatDto = {
    chatId: 1,
    name: 'name',
    creatorId: 1,
    creatorName: 'turin'
};
