import { ChatDto } from '../../models/chat-dto.interface';

export const GetChatByIdMock: ChatDto = {
    chatId: 1,
    name: 'name',
    creatorId: 1,
    creatorName: 'turin'
};
