import { ResponsePaginationModel } from 'src/app/models/response-pagination-model.interface';
import { ChatMessagesDto } from '../../../models/chat-message-dto.interface';

export const GetMessagesMock: ResponsePaginationModel<ChatMessagesDto> = {
    count: 5,
    items: [
      {
          chatId: 1,
          chatMessageId: 1,
          creatorId: 1,
          creatorName: 'turin',
          message: 'message'
      },
      {
          chatId: 1,
          chatMessageId: 3,
          creatorId: 1,
          creatorName: 'turin',
          message: 'message'
      },
      {
        chatId: 1,
        chatMessageId: 2,
        creatorId: 2,
        creatorName: 'turin',
        message: 'message'
      }
      ,
      {
        chatId: 1,
        chatMessageId: 4,
        creatorId: 2,
        creatorName: 'turin',
        message: 'message'
      }
      ,
      {
        chatId: 1,
        chatMessageId: 5,
        creatorId: 1,
        creatorName: 'turin',
        message: 'message'
      }
    ]
};
