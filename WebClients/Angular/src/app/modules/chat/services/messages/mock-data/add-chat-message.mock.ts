import { ChatMessagesDto } from '../../../models/chat-message-dto.interface';

export const AddChatMessageMock: ChatMessagesDto = {
    chatId: 1,
    chatMessageId: 1,
    creatorId: 1,
    creatorName: 'turin',
    message: 'message'
};
