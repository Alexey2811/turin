export const GetChatMessagesUri = 'chatMessages/getMessages';
export const DeleteChatMessageUri = 'chatMessages/deleteMessage';
export const AddChatMessageUri = 'chatMessages/addChatMessages';
