import { TestBed } from '@angular/core/testing';

import { ChatMessageService } from './chat-message.service';
import { WebApiHttpService } from 'src/app/services/web-api-http.service';
import { MockWebApiHttpService } from 'src/app/services/mock-web-api-http.service';

describe('ChatMessageService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
    providers: [
      {
        provide: WebApiHttpService, 
        useClass: MockWebApiHttpService
      },
      ChatMessageService
    ]
  }));

  it('should be created', () => {
    const service: ChatMessageService = TestBed.get(ChatMessageService);
    expect(service).toBeTruthy();
  });

  it('should be addChatMessages', (done: DoneFn) => {
    const service: ChatMessageService = TestBed.get(ChatMessageService);

    const res = service.addChatMessages({
      creatorId: 1,
      chatId: 1,
      message: 'message'
    });
    res.subscribe(obj => {
      expect(obj.creatorName).toEqual('turin');
      done();
    });
  });

  it('should be deleteMessage', (done: DoneFn) => {
    const service: ChatMessageService = TestBed.get(ChatMessageService);

    const res = service.deleteMessage(1);
    res.subscribe(obj => {
      expect(obj).toBeNull();
      done();
    });
  });

  it('should be getMessages', (done: DoneFn) => {
    const service: ChatMessageService = TestBed.get(ChatMessageService);

    const res = service.getMessages({
      chatId: 1,
      page: 0,
      size: 10
    });
    res.subscribe(obj => {
      expect(obj.count).toBeGreaterThan(0);
      done();
    });
  });
});
