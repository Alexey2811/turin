import { Injectable } from '@angular/core';
import { WebApiHttpService } from 'src/app/services/web-api-http.service';
import { RequestPaginationWithChatIdModel } from '../../models/request-Pagination-with-chatid-model.interface';
import { GetChatMessagesUri, AddChatMessageUri, DeleteChatMessageUri } from './uri.consts';
import { AddChatMessageModel } from '../../models/add-chat-message-model.interface';
import { Observable } from 'rxjs';
import { ResponsePaginationModel } from 'src/app/models/response-pagination-model.interface';
import { ChatMessagesDto } from '../../models/chat-message-dto.interface';

@Injectable()
export class ChatMessageService {

  constructor(private readonly http: WebApiHttpService) { }

  getMessages = (request: RequestPaginationWithChatIdModel): Observable<ResponsePaginationModel<ChatMessagesDto>> =>
    this.http.post(GetChatMessagesUri, request)

  addChatMessages = (request: AddChatMessageModel): Observable<ChatMessagesDto> =>
    this.http.put(AddChatMessageUri, request)

  deleteMessage = (id: number): Observable<void> =>
    this.http.delete(DeleteChatMessageUri, {messageId: id})
}
