import { Injectable } from '@angular/core';
import { WebApiHttpService } from 'src/app/services/web-api-http.service';
import { RequestPaginationWithUserIdModel } from '../models/request-Pagination-with-userid-model.interface';
import { GetMyChatsUri, GetAllChatsUri, DeleteChatUri, AddChatUri, GetChatByIdUri, UpdateChatUri } from './uri.consts';
import { RequestPaginationModel } from 'src/app/models/request-Pagination-model.interface';
import { Observable } from 'rxjs';
import { ResponsePaginationModel } from 'src/app/models/response-pagination-model.interface';
import { ChatDto } from '../models/chat-dto.interface';
import { AddChatModel } from '../models/add-chat-model.interface';
import { UpdateChatModel } from '../models/update-chat-model.interface';

@Injectable()
export class ChatService {

  constructor(private readonly http: WebApiHttpService) { }

  getMyChats = (request: RequestPaginationWithUserIdModel): Observable<ResponsePaginationModel<ChatDto>> =>
    this.http.post(GetMyChatsUri, request)

  getAllChats = (request: RequestPaginationModel): Observable<ResponsePaginationModel<ChatDto>> =>
    this.http.post(GetAllChatsUri, request)

  deleteChat = (id: number): Observable<void> =>
    this.http.delete(DeleteChatUri, {chatId: id})

  addChat = (model: AddChatModel): Observable<ChatDto> =>
    this.http.put(AddChatUri, model)

  updateChat = (model: UpdateChatModel): Observable<void> =>
    this.http.post(UpdateChatUri, model)

  getChatById = (id: number): Observable<ChatDto> =>
    this.http.get(GetChatByIdUri, {chatId: id})
}
