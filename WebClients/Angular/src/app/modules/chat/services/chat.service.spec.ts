import { TestBed } from '@angular/core/testing';

import { ChatService } from './chat.service';
import { WebApiHttpService } from 'src/app/services/web-api-http.service';
import { MockWebApiHttpService } from 'src/app/services/mock-web-api-http.service';

describe('ChatService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
    providers: [
      {
        provide: WebApiHttpService, 
        useClass: MockWebApiHttpService
      },
      ChatService
    ]
  }));

  it('should be created', () => {
    const service: ChatService = TestBed.get(ChatService);
    expect(service).toBeTruthy();
  });

  it('should be addChat', (done: DoneFn) => {
    const service: ChatService = TestBed.get(ChatService);

    const res = service.addChat({
      name: 'name',
      creatorId: 1
    });
    res.subscribe(obj => {
      expect(obj.name).toEqual('name');
      done();
    });
  });

  it('should be deleteChat', (done: DoneFn) => {
    const service: ChatService = TestBed.get(ChatService);

    const res = service.deleteChat(1);
    res.subscribe(obj => {
      expect(obj).toBeNull();
      done();
    });
  });

  it('should be getAllChats', (done: DoneFn) => {
    const service: ChatService = TestBed.get(ChatService);

    const res = service.getAllChats({
      page: 0,
      size: 10
    });
    res.subscribe(obj => {
      expect(obj.count).toBeGreaterThan(0);
      done();
    });
  });

  it('should be getChatById', (done: DoneFn) => {
    const service: ChatService = TestBed.get(ChatService);

    const res = service.getChatById(1);
    res.subscribe(obj => {
      expect(obj).toBeTruthy();
      done();
    });
  });

  it('should be getMyChats', (done: DoneFn) => {
    const service: ChatService = TestBed.get(ChatService);

    const res = service.getMyChats({
      page: 0,
      size: 10,
      userId: 1
    });
    res.subscribe(obj => {
      expect(obj.count).toBeGreaterThan(0);
      done();
    });
  });

  it('should be updateChat', (done: DoneFn) => {
    const service: ChatService = TestBed.get(ChatService);

    const res = service.updateChat({
      chatId: 1,
      chatName: 'newName'
    });
    res.subscribe(obj => {
      expect(obj).toBeNull();
      done();
    });
  });
});
