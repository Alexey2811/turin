import { Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { ConnectUriChatHub } from './uri.consts';
import { ChatMessagesDto } from '../../models/chat-message-dto.interface';
import {SetCountUsersMethod, AddMessageMethod, DeleteMessageMethod, DeleteChatMethod} from './response-methods.consts';

@Injectable()
export class WebSocketService {

  private chatConnection: HubConnection;

  constructor() { }

  connect = (chatId: number,
             setCountUsersFn: (count: number) => void,
             addMessageFn: (message: ChatMessagesDto) => void,
             deleteMessageFn: (id: number) => void,
             deleteChatFn: () => void) => {

    this.chatConnection = new HubConnectionBuilder()
      .withUrl(`${ConnectUriChatHub}?chatId=${chatId}`)
      .build();

    this.chatConnection
      .on(SetCountUsersMethod, setCountUsersFn);

    this.chatConnection
      .on(AddMessageMethod, addMessageFn);

    this.chatConnection
      .on(DeleteMessageMethod, deleteMessageFn);

    this.chatConnection
      .on(DeleteChatMethod, deleteChatFn);

    this.chatConnection
      .start()
      .then(() => console.log('Connection started'))
      .catch(err => console.log('Error while starting connection: ' + err));
  }

  disconnect = () => {
    this.chatConnection.stop();
  }
}
