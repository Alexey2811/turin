import { TestBed } from '@angular/core/testing';

import { WebSocketService } from './web-socket.service';
import { WebApiHttpService } from 'src/app/services/web-api-http.service';
import { MockWebApiHttpService } from 'src/app/services/mock-web-api-http.service';
import { MockWebSocketService } from './mock-web-socket.service';
import { ChatMessagesDto } from '../../models/chat-message-dto.interface';

describe('WebSocketService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {
        provide: WebApiHttpService,
        useClass: MockWebApiHttpService
      },
      {
        provide: WebSocketService,
        useClass: MockWebSocketService
      }
    ]
  }));

  it('should be created', () => {
    const service: WebSocketService = TestBed.get(WebSocketService);
    expect(service).toBeTruthy();
  });

  it('should be connect', () => {
    const service: WebSocketService = TestBed.get(WebSocketService);

    const setCountUser = (id: number) => {};
    const addMessage = (model: ChatMessagesDto) => {};
    const deleteMessage = (id: number) => {};
    const deleteChat = () => {};

    service.connect(1, setCountUser, addMessage, deleteMessage, deleteChat);
    expect(service).toBeTruthy();
  });

  it('should be disconnect', () => {
    const service: WebSocketService = TestBed.get(WebSocketService);

    service.disconnect();
    expect(service).toBeTruthy();
  });
});
