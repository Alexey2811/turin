import { environment } from 'src/environments/environment';

export const ConnectUriChatHub = `${environment.api}${environment.chatHub}`;
