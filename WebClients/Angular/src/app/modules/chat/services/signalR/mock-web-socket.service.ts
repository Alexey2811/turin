import { ChatMessagesDto } from '../../models/chat-message-dto.interface';

export class MockWebSocketService {

  constructor() { }

  connect = (chatId: number,
             setCountUsersFn: (count: number) => void,
             addMessageFn: (message: ChatMessagesDto) => void,
             deleteMessageFn: (id: number) => void) => {
  }

  disconnect = () => {
  }
}
