export const GetAllChatsUri = 'chats/getAllChats';
export const GetMyChatsUri = 'chats/getMyChats';
export const GetChatByIdUri = 'chats/getChatById';
export const UpdateChatUri = 'chats/updateChat';
export const AddChatUri = 'chats/addChat';
export const DeleteChatUri = 'chats/deleteChat';
