import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexChatComponent } from './components/index-chat/index-chat.component';


const routes: Routes = [
  {
    path: '',
    component: IndexChatComponent
  },
  {
    path: ':page',
    component: IndexChatComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChatsRoutingModule { }
