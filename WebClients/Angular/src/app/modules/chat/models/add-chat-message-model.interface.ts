export interface AddChatMessageModel {
    chatId: number;
    creatorId: number;
    message: string;
}
