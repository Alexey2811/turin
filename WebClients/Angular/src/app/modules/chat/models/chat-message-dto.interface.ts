export interface ChatMessagesDto {
    chatMessageId: number;
    creatorId: number;
    creatorName: string;
    chatId: number;
    message: string;
}
