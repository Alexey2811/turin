export interface ChatDto {
    chatId: number;
    name: string;
    creatorId: number;
    creatorName: string;
}
