import { RequestPaginationModel } from 'src/app/models/request-Pagination-model.interface';

export interface RequestPaginationWithUserIdModel extends RequestPaginationModel {
    userId: number;
}
