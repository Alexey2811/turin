export interface UpdateChatModel {
    chatId: number;
    chatName: string;
}
