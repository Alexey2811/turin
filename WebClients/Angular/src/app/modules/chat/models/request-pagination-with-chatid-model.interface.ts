import { RequestPaginationModel } from 'src/app/models/request-Pagination-model.interface';

export interface RequestPaginationWithChatIdModel extends RequestPaginationModel {
    chatId: number;
}
