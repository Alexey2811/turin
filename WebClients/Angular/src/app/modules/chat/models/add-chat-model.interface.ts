export interface AddChatModel {
    name: string;
    creatorId: number;
}
