import { AbstractControl, ValidationErrors, AsyncValidatorFn } from '@angular/forms';
import { Observable, of, } from 'rxjs';
import { map } from 'rxjs/operators';

export const AsyncIsLoginExists = (
    fn: (login: string) => Observable<boolean>,
    oldLogin?: string
    ): AsyncValidatorFn => {

        return (control: AbstractControl): Observable<ValidationErrors | null> => {

            const error: ValidationErrors = {
                login_is_busy: { value: 'values is not confirmed'}
            };

            const text = control.value;

            if (oldLogin && text === oldLogin) {
                return of (null);
            }

            return fn(text).pipe(
                map(val => {
                    return val ? error : null;
                })
            );
        };
};
