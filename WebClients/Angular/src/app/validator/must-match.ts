import { FormGroup, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';

export const MustMatch = (group: FormGroup, second: string): ValidatorFn  => 
    (control: AbstractControl): ValidationErrors | null => {
        let a: string;

        if (group){
            a = group.get(second).value;
        }
        let b = control.value;

        if (a==b && a && b) return null;

        return {
            'confirmError': { value: 'values is not confirmed'}
        }
    }
