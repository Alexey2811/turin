export interface UpdateUserModel {
    userId: number;
    login: string;
    password: string;
}
