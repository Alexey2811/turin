export interface RoleDto {
    roleId: number;
    name: string;
}
