import { RoleDto } from './role-dto.interface';

export interface UserDto {
    userId: number;
    login: string;
    roles: Array<RoleDto>;
}
