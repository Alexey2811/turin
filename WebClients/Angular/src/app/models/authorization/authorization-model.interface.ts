export interface AuthorizationModel {
    login: string;
    password: string;
}
