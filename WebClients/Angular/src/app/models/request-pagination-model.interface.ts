export interface RequestPaginationModel {
    page: number;
    size: number;
}
