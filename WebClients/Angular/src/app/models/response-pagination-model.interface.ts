export interface ResponsePaginationModel<T> {
    count: number;
    items: Array<T>;
}
