export interface ErrorFormatModel {
    errorCode: number;
    message: string;
}
