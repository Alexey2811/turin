import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthorizationService } from 'src/app/services/authorization/authorization.service';
import { AuthorizationModel } from 'src/app/models/authorization/authorization-model.interface';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  form: FormGroup;

  constructor(private readonly bsModalRef: BsModalRef,
              private readonly auth: AuthorizationService) {
                
    this.form = new FormGroup({
      login: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      password: new FormControl('', [Validators.required, Validators.maxLength(100)])
    });
  }

  ngOnInit() {
  }

  close() {
    this.bsModalRef.hide();
  }

  signIn() {

    if (this.form.valid) {
      const authModel: AuthorizationModel = {
        login: this.form.get('login').value,
        password: this.form.get('password').value
      };

      this.auth.signIn(authModel).subscribe(_ => {
        location.reload();
      });
    }
  }
}
