import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { UserService } from 'src/app/services/users/user.service';
import { TranslateService } from '@ngx-translate/core';
import {
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';
import { AuthorizationService } from 'src/app/services/authorization/authorization.service';
import { AsyncIsLoginExists } from 'src/app/validator/async-is-login-exists';
import { MustMatch } from 'src/app/validator/must-match';
import { WebApiHttpService } from 'src/app/services/web-api-http.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  form: FormGroup;

  showSuccess = false;

  successProfile = '';

  constructor(private readonly bsModalRef: BsModalRef,
              private readonly user: UserService,
              private readonly tr: TranslateService,
              private readonly auth: AuthorizationService,
              private readonly web: WebApiHttpService) {

      this.form = new FormGroup({
        login: new FormControl('',
          [Validators.required, Validators.maxLength(100)],
          [
            AsyncIsLoginExists(this.user.isExistUser, this.auth.user ?
              this.auth.user.login : null)
          ]),
        password: new FormControl('', [Validators.required, Validators.maxLength(100)]),
        confirmed_password: new FormControl('', [Validators.required])
      });

      this.form.get('confirmed_password').setValidators(MustMatch(this.form, 'password'));
      this.tr.get('success_profile_string').subscribe(text => { this.successProfile = text; });
  }

  ngOnInit() {
  }

  close() {
    this.bsModalRef.hide();
  }

  updateProfile() {
    const { form, user, web, auth } = this;

    if (form.valid) {
      user.updateUser({
        login: this.form.get('login').value,
        password: this.form.get('password').value,
        userId: auth.user.userId
      }).subscribe(tokenModel => {

        if (tokenModel) {
          web.token = tokenModel.token;

          auth.user = {
            ...auth.user,
            login: this.form.get('login').value,
          };

          this.showSuccess = true;
        }
      });
    }
  }
}
