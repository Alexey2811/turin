import {Component, OnInit} from '@angular/core';
import {TranslateServiceWebApi} from 'src/app/services-ui/translate-service-web-api.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {SignInComponent} from '../sign-in/sign-in.component';
import {SignUpComponent} from '../sign-up/sign-up.component';
import {ProfileComponent} from '../profile/profile.component';
import {UserResolverService} from '../../resolvers/user-resolver.service';
import {AuthorizationService} from '../../services/authorization/authorization.service';
import {WebApiHttpService} from '../../services/web-api-http.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.scss']
})
export class NavMenuComponent implements OnInit {

  constructor(private readonly tr: TranslateServiceWebApi,
              private readonly modalService: BsModalService,
              public readonly auth: AuthorizationService,
              private readonly web: WebApiHttpService,
              private readonly router: Router) {

    this.auth.login.subscribe(value => {
      this.disabledLink = !value;
    });
  }

  navBarOpen = false;
  bsModalRef: BsModalRef;

  disabledLink = false;

  toggleNavBar() {
    this.navBarOpen = !this.navBarOpen;
  }

  ngOnInit() {
  }

  setLang = (lang: 'en' | 'ru') =>
    this.tr.changeLang(lang);

  openSignIn() {
    this.bsModalRef = this.modalService.show(SignInComponent, {
      ignoreBackdropClick: true
    });
  }

  openSignUp() {
    this.bsModalRef = this.modalService.show(SignUpComponent, {
      ignoreBackdropClick: true
    });
  }

  openProfile() {
    this.bsModalRef = this.modalService.show(ProfileComponent, {
      ignoreBackdropClick: true
    });
  }

  signOut() {
    const {auth, router} = this;

    auth.signOut(this.web.token).subscribe(_ => {
      this.disabledLink = true;
      router.navigate(['/']);
    });
  }
}
