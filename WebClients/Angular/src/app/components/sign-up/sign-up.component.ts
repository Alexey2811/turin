import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MustMatch } from 'src/app/validator/must-match';
import { AsyncIsLoginExists } from 'src/app/validator/async-is-login-exists';
import { UserService } from 'src/app/services/users/user.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  form: FormGroup;

  showSuccess = false;

  success_sign_up_string = '';

  constructor(private readonly bsModalRef: BsModalRef,
    private readonly user: UserService,
    private readonly tr: TranslateService) {

    this.form = new FormGroup({
      login: new FormControl('',
        [Validators.required, Validators.maxLength(100)],
        [
          AsyncIsLoginExists(this.user.isExistUser)
        ]),
      password: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      confirme_password: new FormControl('', [Validators.required])
    });

    this.form.get('confirme_password').setValidators(MustMatch(this.form, 'password'));
    this.tr.get('success_sign_in').subscribe(text => {
      this.success_sign_up_string = text;
    });
  }

  ngOnInit() {
  }

  close() {
    this.bsModalRef.hide();
  }

  signUp() {
    if (this.form.valid) {
      this.user.addUser({
        login: this.form.get('login').value,
        password: this.form.get('password').value
      }).subscribe(userDto => {
        if (userDto) {
          this.showSuccess = true;
        }
      });
    }
  }
}
