import { environment } from 'src/environments/environment';

export class UriBuilder {
    static get apiAddress(): string {
        return `${environment.api}api/`;
    }
}
