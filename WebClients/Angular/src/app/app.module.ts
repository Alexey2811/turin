import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WebApiHttpService } from './services/web-api-http.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AuthorizationService } from './services/authorization/authorization.service';
import { UserService } from './services/users/user.service';
import { environment } from 'src/environments/environment';
import { MockWebApiHttpService } from './services/mock-web-api-http.service';
import { IndexAppComponent } from './components/index-app/index-app.component';
import { TranslateServiceWebApi } from './services-ui/translate-service-web-api.service';
import { SharedModule } from './modules/shared/shared.module';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ErrorComponent } from './components/error/error.component';
import { UserResolverService } from './resolvers/user-resolver.service';
import { CanActiveService } from './can-active/can-active.service';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AlertModule, 
  BsDropdownModule, 
  ModalModule, 
  PaginationModule, 
  BsModalRef } from 'ngx-bootstrap';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    IndexAppComponent,
    NavMenuComponent,
    SignInComponent,
    SignUpComponent,
    ProfileComponent,
    ErrorComponent
  ],
  entryComponents: [
    SignInComponent,
    SignUpComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    SharedModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    AlertModule.forRoot(),
    PaginationModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    {
      provide: WebApiHttpService,
      useClass: environment.mock ?
        MockWebApiHttpService : WebApiHttpService
    },
    AuthorizationService,
    UserService,
    TranslateServiceWebApi,
    UserResolverService,
    BsModalRef,
    CanActiveService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
