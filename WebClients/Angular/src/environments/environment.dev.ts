export const environment = {
  production: false,
  mock: false,
  api: 'http://localhost:5000/',
  chatHub: 'chatHub'
};
