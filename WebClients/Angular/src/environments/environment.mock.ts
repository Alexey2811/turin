export const environment = {
  production: false,
  mock: true,
  api: 'http://localhost:5000/',
  chatHub: 'chatHub'
};
