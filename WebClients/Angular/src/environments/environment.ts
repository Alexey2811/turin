export const environment = {
  production: true,
  mock: false,
  api: 'http://localhost:5000/',
  chatHub: 'chatHub'
};
